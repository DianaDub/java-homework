package examples.from_google;

public class AbstractClass {
    public static void main(String[] args) {
        Employee john = new Employee("John", "Some company");
        john.display();
        Customer jane = new Customer("Jane", "Some another company");
        jane.display();
    }
}

abstract class Human {
    private String name;

    public String getName() { return name; }

    public Human(String name){
        this.name=name;
    }

    public abstract void display();
}

class Employee extends Human {
    private String insuranceCompany;

    public Employee(String name, String company) {
        super(name);
        this.insuranceCompany = company;
    }

    public void display(){

        System.out.printf("Employee Name: %s \t Insurance company: %s \n", super.getName(), insuranceCompany);
    }
}

class Customer extends Human {
    private String insuranceCompany;

    public Customer(String name, String company) {
        super(name);
        this.insuranceCompany = company;
    }
    public void display(){

        System.out.printf("Customer Name: %s \t Insurance company: %s \n", super.getName(), insuranceCompany);
    }
}
