package examples.from_google;

public class CalculatorWithMethod {

    public static void main(String[] args) {
        double result1 = sum(99.0, 1.0);
        System.out.println(result1);
        double result2 = division(100.0, 1.0);
        System.out.println(result2);
        double result3 = minus(100.0, 100.0);
        System.out.println(result3);
        double result4 = multiplication(1.0, 0.0);
        System.out.println(result4);
    }
    public static double sum(double a, double b) {
        double result = a + b;
        return result;
    }
    public static double minus (double a, double b) {
        double result = a - b;
        return result;
    }
    public static double multiplication (double a, double b) {
        double result = a * b;
        return result;
    }
    public static double division (double a, double b) {
        double result = a / b;
        return result;
    }
}