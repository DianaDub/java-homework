package examples.lessons.ls8.task1;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Task1 task1 = new Task1();
        int a = task1.amountOfWords();
        System.out.println(a);
    }
    public int amountOfWords () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, insert sentence: " );
        String sentence = scanner.nextLine();
        int amount = sentence.split(" ").length;
        System.out.println("Amount of words in your sentence is: " + amount);
        return amount;
    }
    /*
1)Написати метод, який буде запитувати у користувача ввести
з клавіатури речення,
а після вивести в консоль із кількох слів складається дане речення.
А також цей метод повинен повернути кількість цих слів.*/
}
