package examples.lessons.ls8.service;

public class Service {
    public Service(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
    public void needToDoService(City city, Contract contract) {
        if (city.getDistance() > contract.getHuman().getAuto().getDistanceBeforeService()){
            System.out.println("Шановний " + contract.getHuman().getName() + ".\n" +
                    "Перед тим як ви поїдете в " + city.getName() + " вам потрібно пройти ТО.");
        }
        else {
            System.out.println("Шановний " + contract.getHuman().getName() + ".\n" +
                    "Їдьте в " + city.getName() + ".\nАле не забувайте, що згідно договору " + contract.getId() +
                    " ви обслуговуєтесь у нас.\n" + "Ваш " + name + ".");
        }
    }
}
/* Автомобіль закріплений за певною людиною.
   Якщо при поїздці в певне місто, пробіг після фінального ТО більше, ніж відстань,
   після проїзду якої автомобіль повинен пройти ТО. На консоль виводиться повідомлення типу:
   Шановний {Ім'я людини}.
   Перед тим як ви поїдете в {Назва міста} вам потрібно пройти ТО.
   Шановний {Імя людини}.
   Їдьте в {Назва міста}. Але не забувайте, що згідно договору {айді договору} ви обслуговуєтесь у нас.
   Ваш {Назва Сервісу}.*/
