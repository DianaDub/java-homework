package examples.lessons.ls8.service;

public class Human {
    private String name;

    public String getName() {
        return name;
    }

    public Auto getAuto() {
        return auto;
    }

    private Auto auto;

    public Human(String name, Auto auto) {
        this.name = name;
        this.auto = auto;
    }
}
