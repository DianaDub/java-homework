package examples.lessons.ls8.service;

public class Contract {
    private int id;

    public int getId() {
        return id;
    }

    public Human getHuman() {
        return human;
    }

    public Contract(int id, Human human) {
        this.id = id;
        this.human = human;
    }

    private Human human;
}
