package examples.lessons.ls8.service;

public class TaskToDescription {
    public static void main(String[] args) {
        Auto auto = new Auto("Audi", 20000, 3000);
        Human human = new Human("Mark", auto);
        Contract contract = new Contract(11111, human);
        City city = new City("Kyiv", 600);
        Service service = new Service("ServiceNew");
        service.needToDoService(city, contract);
        System.out.println();
        Auto auto1 = new Auto("Volkswagen", 20000, 300);
        Human human1 = new Human("Elya", auto1);
        Contract contract1 = new Contract(12345, human1);
        City city1 = new City("Zhytomyr", 600);
        Service service1 = new Service("ServiceNew");
        service.needToDoService(city1, contract1);
        System.out.println();
        new Service("ServiceNew").needToDoService(new City(
                "Kyiv", 3000), new Contract(00000, new Human("Mira",
                new Auto("Audi", 21000, 2100))));
    }
    /*
    Написати метод за допомогою об'єктного підходу.
   Який буде виводити повідомлення про те, чи потрібно людині, яка володіє автомобілем і має договір
   з автосервісом перед поїздкою в яке-небудь місто пройти ТО.
   Рішення про те, чи потрібно людині пройти ТО, відбувається в атосервісі.
   Для цього у людини повинен бути укладений контракт із сервісом.
   У контракті міститься id цього контракту, і закріплений до цієї людини
   з визначеним іменем.
   У кожного автомобіля є ціна, замовлення, відстань до проходження ТО.

   Автомобіль закріплений за певною людиною.
   Якщо при поїздці в певне місто, пробіг після фінального ТО більше, ніж відстань,
   після проїзду якої автомобіль повинен пройти ТО. На консоль виводиться повідомлення типу:
   Шановний {Ім'я людини}.
   Перед тим як ви поїдете в {Назва міста} вам потрібно пройти ТО.
   Шановний {Імя людини}.
   Їдьте в {Назва міста}. Але не забувайте, що згідно договору {айді договору} ви обслуговуєтесь у нас.
   Ваш {Назва Сервісу}.*/
}
