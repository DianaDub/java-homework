package examples.lessons.ls8.service;

public class Auto {
    private String name;
    private int price;

    @Override
    public String toString() {
        return "Auto{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", distanceBeforeService=" + distanceBeforeService +
                '}';
    }

    public int getDistanceBeforeService() {
        return distanceBeforeService;
    }

    public void setDistanceBeforeService(int distanceBeforeService) {
        this.distanceBeforeService = distanceBeforeService;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    private int distanceBeforeService;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Auto(String name, int price, int distanceBeforeService) {
        this.name = name;
        this.price = price;
        this.distanceBeforeService = distanceBeforeService;
    }


}
