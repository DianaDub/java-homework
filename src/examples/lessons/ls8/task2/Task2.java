package examples.lessons.ls8.task2;

import java.util.Date;
import java.util.SplittableRandom;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("My age is " + new Task2().returnMyCurrentAge(2002));
        System.out.println("My age is " + new Task2().returnMyCurrentAge2(1111));
    }
    public int returnMyCurrentAge (int yearOfBirth) {
        Date date = new Date();
        String yearNow = String.format("%tY", date);
        int currentYear = Integer.parseInt(yearNow);
        int myCurrentAge = currentYear - yearOfBirth;
        return myCurrentAge;
    }
    public int returnMyCurrentAge2 (int yearOfBirth) {
        return Integer.parseInt(String.format("%tY", new Date())) - yearOfBirth;
    }
/* 2) Написати метод, який в параметрах приймає ваш рік народження.
Цей метод повертає int, який відповідає вашому віку.
Використовуємо клас Дата як на минулому занятті.
 (P.s поки не чіпаємо месяць народження,
зупинимось на різниці у роках.)*/
}
