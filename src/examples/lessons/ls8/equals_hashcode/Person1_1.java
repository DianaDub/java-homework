package examples.lessons.ls8.equals_hashcode;

public class Person1_1 {
    public static void main(String[] args) {
        Person person1 = new Person("Mark", 21, "33333");
        Person person2 = new Person("Emma", 19, "11111");
        Person person3 = new Person("Emma", 19, "00000");

        System.out.println(person1);
        System.out.println(person2);
        boolean compare = person1.equals(person2);
        System.out.println(compare);
        System.out.println(person1.hashCode());
        System.out.println(person2.hashCode());
        System.out.println(person3.hashCode());
        System.out.println(person2.equals(person3));
    }
}
