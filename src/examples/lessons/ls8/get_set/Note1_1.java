package examples.lessons.ls8.get_set;

public class Note1_1 {
    public static void main(String[] args) {
        Note note = new Note();
        System.out.println(note.getName());
        System.out.println(note.getCountOfPages());
        note.setCountOfPages(111);
        note.setName("Eleven");
        System.out.println(note.getName());
        System.out.println(note.getCountOfPages());
    }

}
