package examples.lessons.ls8.get_set;

public class Note {
    private String name;
    private int countOfPages;
    public String getName(){
        return name;
    }
    public int getCountOfPages(){
        return countOfPages;
    }
   /* public void setName(String valueName) {
        name = valueName;
        }
      public void setCountOfPages(int valueCountOfPages) {
      countOfPages = valueCountOfPages;   - так не пишуть, пишуть так:
    } */
    public void setName (String name) {
        this.name = name;
    }
    public void setCountOfPages(int countOfPages) {
        this.countOfPages = countOfPages;
    }
}
