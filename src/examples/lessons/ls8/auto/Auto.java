package examples.lessons.ls8.auto;

import java.lang.reflect.AnnotatedArrayType;

public class Auto {
    public static void main(String[] args) {
        Auto auto = new Auto("Audi", 1000000, true);
    }
/*4)Створити клас Авто. В цьому класі створити поле Ім'я рядка, ціна int, логічне значення isNew
створення геттерів і сеттерів для даних полів, а також конструктор класу,
приймаючи в параметрах всі поля даного класу.*/
    private String name;
    private int price;
    private boolean isNew;
    public String getName(){
        return name;
    }
    public int getPrice(){
        return price;
    }
    public boolean isNew(){
        return isNew;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }
    public Auto (String name, int price, boolean isNew) {
        this.name = name;
        this.price = price;
        this.isNew = isNew;
    }
}
