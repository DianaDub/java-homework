package examples.lessons.ls8.auto_to_string;

public class AutoToString {
    public static void main(String[] args) {
        AutoToString auto = new AutoToString("Audi", 1000000, true);
        System.out.println(auto);
        String autoInfo = auto.toString();
        System.out.println(autoInfo);
    }
    @Override
    public String toString (){
        if(isNew){
            return "Auto with name " + name + " costs " + price + ". Car is new!";
        }else {
            return "Auto with name " + name + " costs " + price + ". Car is not new!";
        }
    }


    /*4)Створити клас Авто. В цьому класі створити поле Ім'я рядка, ціна int, логічне значення isNew
    створення геттерів і сеттерів для даних полів, а також конструктор класу,
    приймаючи в параметрах всі поля даного класу.*/
    private String name;
    private int price;
    private boolean isNew;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public AutoToString(String name, int price, boolean isNew) {
        this.name = name;
        this.price = price;
        this.isNew = isNew;
    }
}
