package examples.lessons.ls8.constructor;

public class House1_1 {
    public static void main(String[] args) {
        House house1 = new House();
        house1.getAllInfoAboutHouse();
        System.out.println();
        house1.setAddress("Fine");
        house1.setCapacity(5);
        house1.setCountOfFloors(2);
        house1.setNew(true);
        house1.getAllInfoAboutHouse();

        System.out.println();
        House house2 = new House("Fine", 5, 2, false);
        house2.getAllInfoAboutHouse();
        System.out.println();

        House house3 = new House(5, false);
        house3.getAllInfoAboutHouse();
    }

}
