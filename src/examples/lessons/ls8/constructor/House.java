package examples.lessons.ls8.constructor;

public class House {
    public House(String address, int capacity, int countOfFloors, boolean isNew) {
        this.isNew = isNew;
        this.capacity = capacity;
        this.countOfFloors = countOfFloors;
        this.address = address;
    }
    public House(String address, int countOfFloors, boolean isNew, int capacity) {
        this(capacity, isNew);
        this.countOfFloors = countOfFloors;
        this.address = address;
    }

    public House() {
    }

    public House(int capacity, boolean isNew) {
        this.isNew = isNew;
        this.capacity = capacity;
    }



    private String address;

    public boolean isNew() {
        return isNew;
    }
    public void setNew(boolean aNew) {
        isNew = aNew;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public int getCapacity() {
        return capacity;
    }
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
    private int capacity;
    private int countOfFloors;
    public int getCountOfFloors() {
        return countOfFloors;
    }
    public void setCountOfFloors(int countOfFloors) {
        this.countOfFloors = countOfFloors;
    }
    private boolean isNew;
    public void getAllInfoAboutHouse () {
        System.out.println("Your address is " + address);
        System.out.println("Your house's capacity is " + capacity);
        System.out.println("Your house has " + countOfFloors + " floors");
        if (isNew) {
            System.out.println("Your house is new");
        }
        else {
            System.out.println("Your house is not new");
        }
    }
}
