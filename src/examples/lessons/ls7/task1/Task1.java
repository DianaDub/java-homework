package examples.lessons.ls7.task1;

public class Task1 {
    //Написати метод, що приймає в якості параметрів два числа,
//який буде виводити на консоль суму цих двох чисел.
// приймає - не повертає
    public void methodForFirstTask(int a, int b) {
        int sum = a + b;
        System.out.println("Sum of " + a + " and " + b + " equals " + sum + ".");
    }
}


