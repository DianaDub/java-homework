package examples.lessons.ls7.task5;

import java.util.Scanner;

public class Task5 {
    public void printTriangle () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter some number: ");
        int counter = scanner.nextInt();
        for (int i = counter; i > 0; i--) {
            System.out.println();
            for (int j = i; j > 0; j--) {
                System.out.print("*");
            }
        }
    }
    public void printTriangle1 () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter some number: ");
        int counter = scanner.nextInt();
        for (int i = counter; i > 0; i--) {
            System.out.println();
            for (int j = i; j > 0; j--) {
                if (i == counter || j == 1 || i == j) {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
        }
    }
    /*public void printTriangle2 () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter some number: ");
        int counter = scanner.nextInt();
        for (int i = counter; i == counter; i++) {
            System.out.println();
            for (int j = i; j == counter; j++){
                System.out.print("*");
            }
        }
    }*/
    /*Написати метод, який буде це робити!!!
        Необхідно вивести на екран перевернутий прямокутний
        трикутник з прямим кутом зверху в лівій частині.
 Для виведення використовуйте умовні оператори, цикли.
 Команду System.out.println(); System.out.print(); і можна використовувати тільки з одним символом *.
 Користувач вводить з клавіатури число, яке буде вважатися основою трикутника,
 а програма виводить сам трикутник.
 Приклад з числом 7:
*******
******
*****
****
***
**
*

*******
*    *
*   *
*  *
* *
**
*
 */
}
