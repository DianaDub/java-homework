package examples.lessons.ls7.task4;

import java.util.Date;

public class Task4 {
/*Напишіть метод, який буде виводити в консоль який точний час і дані зараз
на вашому пристрої. В форматі такого типу: 12 червня 2022 року, 14:55*/
    public void dateProvider1 (Date currentDate) {
        System.out.printf("%1$td %1$tB %1$tY року, %1$tH:%1$tM\n", currentDate);
    }
    public void dateProvide2 () {
        System.out.printf("%1$td %1$tB %1$tY року, %1$tH:%1$tM\n", new Date());
    }
}
