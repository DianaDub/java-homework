package examples.lessons.ls7.task6;

public class Recipe {
    public static void main(String[] args) {
        Recipe saltedWater = new Recipe();
        saltedWater.getInfo();
        saltedWater.nameOfDish = "Salted water";
        saltedWater.ingredients = new String[]{"Salt", "Water"};
        saltedWater.timeToCook = 0.2;
        saltedWater.getInfo();


    }
    public String nameOfDish;
    public String [] ingredients;
    public double timeToCook;
    public void getInfo() {
        System.out.println("Name of dish is " + nameOfDish + ".");
        System.out.println("Ingredients: ");
        int counter = 1;
        if (ingredients == null) {
            System.out.print("No ingredients");
        } else {
            for (String ingredient : ingredients) {
                System.out.println(counter + ")" + ingredient + ".");
                counter++;
            }
        }
        System.out.println();
        System.out.println("Time to cook is " + timeToCook + " hours.");
    }
}

/*Створити клас Recipe (рецепт). У ньому створіть наступні поля:
 a) Поле nameOfDish типу String
 б) Поле інгредієнтів типу String[]
 в) Поле timeToCook типу double

 У цьому класі створіть метод getInfo(), який виведе всю інформацію про рецепт.
 Створити об'єкт класу Рецепт. Задати всі поля для даного об'єкта. Визвати у об'єкта
 метод getInfo().*/