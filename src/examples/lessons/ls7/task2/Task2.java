package examples.lessons.ls7.task2;

public class Task2 {
//Написати другий метод, який поверне суму двох чисел,
//які він буде приймати в параметрі методу.
//приймає - повертає
    public int methodForSecondTask (int a, int b) {
        int sum = a + b;
        return sum;
    }
}
