package examples.lessons.ls7.task2;

import examples.lessons.ls7.task1.Task1;

public class Task2_1 {
    public static void main(String[] args) {
        Task2 variable = new Task2();
        variable.methodForSecondTask(100,3554);
        System.out.println("Sum of 100 and 1 equals " + variable.methodForSecondTask(100,1) + ".");
    }
}
