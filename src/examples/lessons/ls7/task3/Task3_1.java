package examples.lessons.ls7.task3;

import examples.lessons.ls7.task2.Task2;

public class Task3_1 {
    public static void main(String[] args) {
        int [] array = {43, 89, 10, 0, 100, 11, 99};
        Task3 variable = new Task3();
        variable.printArrayOfIntToConsole(array);
        variable.printArrayOfIntToConsole(new int []{45, 675, 99, 0, 111});
    }
}
