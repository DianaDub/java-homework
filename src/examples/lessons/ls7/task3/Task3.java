package examples.lessons.ls7.task3;

public class Task3 {
   /* Написати метод, який буде виводити масив чисел у консоль
    приймає масив чисел, нічого не повертає*/
    public void printArrayOfIntToConsole (int[] array) {
        StringBuilder stringBuilder = new StringBuilder("[");
        for (int i = 0; i < array.length; i++) {
            stringBuilder.append(array[i]).append(", ");
        }
        stringBuilder.append("]");
        String result = String.valueOf(stringBuilder);
        System.out.println(result.replace(", ]", "]"));
    }
}
