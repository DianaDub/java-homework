package examples.lessons.ls7.task7;

import examples.lessons.ls6.lesson_methods.Methods;

import java.lang.reflect.AnnotatedArrayType;
import java.util.Arrays;
import java.util.Random;

public class OverLoad {
    public static void main(String[] args) {
        OverLoad example = new OverLoad();
        example.doSmth();
        example.doSmth("string");
        example.doSmth(21);
        example.doSmth("text", 111);
        example.doSmth(new int[]{1, 3, 4, 2, 5});
    }
    public void doSmth () {
        System.out.println("I am empty");
    }
    public void doSmth(String a) {
        System.out.println("It is string " + a);;
    }
    public void doSmth (int b) {
        System.out.println("Your number is " + b);
    }
    public void doSmth (String text, int number) {
        System.out.println("Your date is " + text + " and " + number);
    }
    public void doSmth (int[] array) {
        OverLoad some = new OverLoad();
        Methods.arraySort(array);
        System.out.println(Arrays.toString(array));
    }
}
