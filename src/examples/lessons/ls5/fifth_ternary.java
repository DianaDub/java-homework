package examples.lessons.ls5;

import java.util.Scanner;

public class fifth_ternary {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert your number: ");
        int number = scanner.nextInt();
        String result = number > 0 ? "Number is > than 0" : number < 0 ? "Number is < than 0" :
                "Number is 0";
        System.out.println(result);
        scanner.close();
    }
}
