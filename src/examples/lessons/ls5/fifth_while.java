package examples.lessons.ls5;

import java.util.Scanner;

public class fifth_while {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert your number: ");
        int number = scanner.nextInt();
        while (number != 13) {
            if (number % 2 == 0) {
                System.out.println(number);
            }
            System.out.println("Insert your number: ");
            number = scanner.nextInt();
        }
        scanner.close();
    }
}
