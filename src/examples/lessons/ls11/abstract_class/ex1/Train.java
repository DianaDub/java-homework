package examples.lessons.ls11.abstract_class.ex1;

public class Train extends Vehicles {
    public Train(String name, int cost) {
        super(name, cost);
    }

    @Override
    void move() {
        System.out.println("I drive from trainstop to another trainstop");
    }

    @Override
    void takeFuel() {
        System.out.println("It is fuel");
    }
}
