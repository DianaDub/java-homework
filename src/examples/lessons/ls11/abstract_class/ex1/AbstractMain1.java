package examples.lessons.ls11.abstract_class.ex1;

public class AbstractMain1 {
    public static void main(String[] args) {
        //Об'єкта інтерфейсу створити не можна
        /*Sportable sportable = new Sportable() {
            @Override
            public void doSport() {
                System.out.println("Do do do");
            }
        };
        sportable.doSport();*/
        //прописати ось так не вийде, бо це абстрактний клас, а створити абстракцію не вийде
        //Vehicles vehicles = new Vehicles("Plane", 10000000);
        Train train = new Train("Train", 10000);
        train.move();
        train.getInfo();
        System.out.println("Another plane is below");
        AnotherPlane plane1 = new AnotherPlane("Cool", 20000);
        plane1.takeFuel();
        plane1.move();
        plane1.getInfo();
    }
}