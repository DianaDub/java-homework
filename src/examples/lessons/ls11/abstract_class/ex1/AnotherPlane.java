package examples.lessons.ls11.abstract_class.ex1;

public class AnotherPlane extends Plane{
    //Тобто, один метод реалізований у класі plane, а інший у цьому класі
    public AnotherPlane(String name, int cost) {
        super(name, cost);
    }
    @Override
    void takeFuel() {
        System.out.println("This plane is fuel");
    }
}
