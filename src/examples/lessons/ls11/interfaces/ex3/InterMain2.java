package examples.lessons.ls11.interfaces.ex3;

public class InterMain2 implements Inter4{

    @Override
    public void Inter1() {
        System.out.println("Inter1");
    }

    @Override
    public void Inter2() {
        System.out.println("Inter2");
    }

    @Override
    public void Inter3() {
        System.out.println("Inter3");
    }

    @Override
    public void Inter4() {
        System.out.println("Inter4");
    }
}
