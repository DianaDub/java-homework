package examples.lessons.ls11.interfaces.ex1;

public interface Movable {
    //немає тіла методу і не потрібно писати public abstract
    void move();
    void moveBack();
}
