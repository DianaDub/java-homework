package examples.lessons.ls11.interfaces.ex1;

import examples.lessons.ls11.interfaces.ex1.Car;
import examples.lessons.ls11.interfaces.ex1.Fish;

public class InterMain1 {
    public static void main(String[] args) {
        Fish fish = new Fish();
        Car car = new Car();
        fish.move();
        car.move();
        fish.eat();
        fish.defaultMethod();
    }
}

