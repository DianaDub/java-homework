package examples.lessons.ls11.interfaces.ex1;

public class Fish implements Movable, Eatable {

    @Override
    public void move() {
        System.out.println("I am a fish, I can swim");
    }

    @Override
    public void moveBack() {
        System.out.println("I can't swim back");
    }
    public void eat() {
        System.out.println("I eat worms");
    }
}
