package examples.lessons.ls11.interfaces.ex2;

//import examples.lessons.ls6.arraylist.ArrayList;
import java.util.ArrayList;

public class InterTask1 {
    public static void main(String[] args) {
        BodybuildingMan sport1 = new BodybuildingMan("Mike");
        CalisthenicsMan sport2 = new CalisthenicsMan("John");
        CrossFitMan sport3 = new CrossFitMan("Sam");
        Sportable[] arraySport = {sport1, sport2, sport3};
        for (Sportable sportable: arraySport) {
            sportable.doSport();
        }
        System.out.println("Виводить одне і те ж саме");
        Sportable sport4 = new BodybuildingMan("Dean");
        Sportable sport5 = new CalisthenicsMan("Endy");
        Sportable sport6 = new CalisthenicsMan("Liam");
        Sportable[] arraySport1 = {sport4, sport5, sport6};
        for (Sportable sportable1: arraySport1) {
            sportable1.doSport();
        }
        System.out.println("Для створення масиву, який буде розширюватись автоматично, " +
                "потрібно використовувати ArrayList");
        ArrayList <Sportable> sportableArrayList = new ArrayList<>();
        sportableArrayList.add(sport1);
        sportableArrayList.add(sport4);
        sportableArrayList.add(sport6);
        System.out.println(sportableArrayList);
    }
}
/*Створити інтерфейс Sportable, в якому назанати тільки один метод doSport().
Створити клас CrossFitMan реалізуючий Sportable, в якому перевизначити метод doSport(), в якому буде виведено
повідомлення :"Я люблю кросфіт. Моє ім'я {name}.".
Створити клас BodybuildingMan реалізуючий Sportable, в якому перевизначити метод doSport(), в якому буде виведено
повідомлення :"Я люблю тренажерну. Моє ім'я {name}." .
Створити клас CalisthenicsMan реалізуючий Sportable, в якому перевизначити метод doSport(), в якому буде виведено
повідомлення :"Я люблю підтягуватись. Моє ім'я {name}.".
Створити масив типу Sportable. Розмістити в нього спортсменів різного виду. Пройти циклом по всьому масиву і
викликати у кожного спортсмена метод doSport().

P.s. В класах поля зробити приватними. Не забути про конструктори, гетери і сетери. А також методи toString,
hashCode() і equals().*/