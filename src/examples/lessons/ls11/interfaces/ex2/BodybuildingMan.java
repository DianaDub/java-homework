package examples.lessons.ls11.interfaces.ex2;

public class BodybuildingMan implements Sportable {
    private String name;

    @Override
    public String toString() {
        return "BodybuildingMan{" +
                "name='" + name + '\'' +
                '}';
    }

    public BodybuildingMan(String name) {
        this.name = name;
    }

    @Override
    public void doSport() {
        System.out.println("Я люблю тренажерну. Моє ім'я "+name+".");
    }
}
