package examples.lessons.ls11.interfaces.ex2;

public interface Sportable {
    void doSport();
}
