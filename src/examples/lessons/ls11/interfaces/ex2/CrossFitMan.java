package examples.lessons.ls11.interfaces.ex2;

public class CrossFitMan implements Sportable {
    public void doSport(){
        System.out.println("Я люблю кросфіт. Моє ім'я "+name+".");
    }
    private String name;

    @Override
    public String toString() {
        return "CrossFitMan{" +
                "name='" + name + '\'' +
                '}';
    }

    public CrossFitMan(String name) {
        this.name = name;
    }
}
