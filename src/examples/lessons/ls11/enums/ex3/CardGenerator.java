package examples.lessons.ls11.enums.ex3;

import java.util.Random;

public class CardGenerator {
    private enum CardsTypes {
        HEART("♥"), CLUB("♣"), SPADE("♠"), DIAMOND("♦");
        public String type;
        CardsTypes(String type) {
            this.type = type;
        }
    }
    private static CardsTypes getRandomType(){
        CardsTypes [] cardsTypes = CardsTypes.values();
        return cardsTypes[new Random().nextInt(cardsTypes.length)];
    }
    private enum CardValues {
        SIX("SIX"), SEVEN("SEVEN"), EIGHT("EIGHT"), NINE("NINE"), TEN("TEN"), JACK("JACK"), QUEEN("QUEEN"),
        KING("KING"), ACE("ACE");
        public String value;

        CardValues(String value) {
            this.value = value;
        }
    }
    private static CardValues getRandomCardValue() {
        CardValues [] cardValues = CardValues.values();
        return cardValues[new Random().nextInt(cardValues.length)];
    }
    private static class Card {
        public CardsTypes cardsTypes;

        @Override
        public String toString() {
            return cardsTypes.type + cardValues.value;
        }

        private Card(CardsTypes cardsTypes, CardValues cardValues) {
            this.cardsTypes = cardsTypes;
            this.cardValues = cardValues;
        }

        public CardValues cardValues;
    }
    public static void printRandomCard() {
        CardGenerator.Card card = new CardGenerator.Card(CardGenerator.getRandomType(), CardGenerator.getRandomCardValue());
        System.out.println(card);
    }
    //Написати програму генератор випадкової карти, яка буде мати
    //статичний метод printRandomCard() виведення в консоль випадково згенерованої карти.
    //Створити два окремих Enum CardsTypes, CardValues.
    //Типи карток можуть бути чотирма видами ♦, ♠, ♣, ♥.
    // CardValues: SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE;
    //Створити клас Card, який в параметрах конструктора буде приймати аргументи типу CardsTypes, CardValues
}
