package examples.lessons.ls11.enums.ex2;

import java.util.EventListener;

public class ConcertLover {
    private String name;
    private int amountOfMoney;

    public void chooseConcert() {
        if (amountOfMoney < Groups.IMAGINE_DRAGONS.getCostOfTicket()){
            System.out.println(name + ", please, stay at home.");
        } else if (amountOfMoney < Groups.COLDPLAY.getCostOfTicket()) {
            System.out.println(name + ", you can go to the group " + Groups.IMAGINE_DRAGONS + ".");
        } else if (amountOfMoney < Groups.BTS.getCostOfTicket()) {
            System.out.println(name + ", you can go to the group " + Groups.COLDPLAY + ".");
        } else {
            System.out.println(name + ", you can go to every concert.");
        }
    }

    public ConcertLover(String name, int amountOfMoney) {
        this.name = name;
        this.amountOfMoney = amountOfMoney;
    }
}
