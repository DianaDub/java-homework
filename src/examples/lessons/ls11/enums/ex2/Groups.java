package examples.lessons.ls11.enums.ex2;

public enum Groups {
    IMAGINE_DRAGONS("Imagine Dragons", 5000), COLDPLAY("Coldplay", 10000), BTS("BTS", 15000);
    private String name;

    public String getName() {
        return name;
    }

    public int getCostOfTicket() {
        return costOfTicket;
    }

    private int costOfTicket;

    Groups(String name, int costOfTicket) {
        this.name = name;
        this.costOfTicket = costOfTicket;
    }
}
