package examples.lessons.ls11.enums.ex1;

public enum Platforms {
    ANDROID, IOS, WINDOWS_PHONE, WRONG_PLATFORM
}
