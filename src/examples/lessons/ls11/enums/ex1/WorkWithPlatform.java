package examples.lessons.ls11.enums.ex1;

import examples.lessons.ls11.enums.ex1.Platforms;

public class WorkWithPlatform {
    public static void startPlatform(Platforms platforms) {
        if (platforms.equals(Platforms.ANDROID)) {
            System.out.println("Android started");
        } else if (platforms.equals(Platforms.IOS)) {
            System.out.println("IOS started");
        } else if (platforms.equals(Platforms.WINDOWS_PHONE)) {
            System.out.println("WINDOWS_PHONE started");
        } else {
            System.out.println("Your platform is " + Platforms.WRONG_PLATFORM);
        }
    }

    public static void startPlatformCase(Platforms platforms) {
        switch (platforms){
            case IOS -> System.out.println("IOS started");
            case ANDROID -> System.out.println("Android started");
            case WINDOWS_PHONE -> System.out.println("WINDOWS_PHONE started");
            default -> System.out.println("Your platform is " + Platforms.WRONG_PLATFORM);
        }
    }
    public static void main(String[] args) {
        startPlatform(Platforms.WRONG_PLATFORM);
        startPlatformCase(Platforms.WINDOWS_PHONE);
    }
}
