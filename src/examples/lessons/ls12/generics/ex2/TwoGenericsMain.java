package examples.lessons.ls12.generics.ex2;

import examples.lessons.ls9.bear.Bear;

public class TwoGenericsMain {
    public static void main(String[] args) {
        ClassWithTwoGenerics<String, Bear> generics = new ClassWithTwoGenerics<>(new String("Text"),
                new Bear("Mia", false));
        generics.printInfoAboutFirstField();
        generics.printInfoAboutSecondField();
    }
}
