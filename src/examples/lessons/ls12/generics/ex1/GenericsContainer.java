package examples.lessons.ls12.generics.ex1;

public class GenericsContainer <T> {
    private T object;

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public GenericsContainer(T object) {
        this.object = object;
    }
}
