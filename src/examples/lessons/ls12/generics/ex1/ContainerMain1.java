package examples.lessons.ls12.generics.ex1;

public class ContainerMain1 {
    public static void main(String[] args) {
        //так працювати не зручно
        Container container1 = new Container("Text");
        Container container2 = new Container("Text2");
        container1.setObject(33);
        //якщо однаковий тип даних, можна зробити кастинг, але якщо тип даних різний, то це не спрацює
        //String result = (String) container1.getObject() + (String) container2.getObject();
        String result = "";
        //перевірити чи є стрінгом
        if (container1.getObject() instanceof String && container2.getObject() instanceof String) {
            result = (String) container1.getObject() + (String) container2.getObject();
        } else {
            System.out.println("Different types");
        }
        System.out.println(result);
        //як це робити за допомогою generics
        GenericsContainer<Integer> genericsContainer = new GenericsContainer<>(23);
        GenericsContainer<Integer> genericsContainer1= new GenericsContainer<>(99);
        GenericsContainer<String> genericsContainer2 = new GenericsContainer<>("Text");
        GenericsContainer<String> genericsContainer3 = new GenericsContainer<>("Text2");
        int resultInt = genericsContainer.getObject() + genericsContainer1.getObject();
        String resultString2 = genericsContainer2.getObject() + genericsContainer3.getObject();
        System.out.println(resultInt);
        System.out.println(resultString2);
        //потрібно такі ж самі типи даних вводити, які були попередньо
        genericsContainer3.setObject("Normal");
    }
}
