package examples.lessons.ls12.generics.ex5;

public class ClassWithTwoGenerics <S, M> {
    private S firstType;
    public M secondType;

    public S getFirstType() {
        return firstType;
    }

    public M getSecondType() {
        return secondType;
    }

    public ClassWithTwoGenerics(S firstType, M secondType) {
        this.firstType = firstType;
        this.secondType = secondType;
    }
    public void printInfoAboutFirstField() {
        System.out.println("Type of this field is " + firstType.getClass().getName());
        System.out.println(firstType);
    }
    public void printInfoAboutSecondField() {
        System.out.println("Type of this field is " + secondType.getClass().getName());
        System.out.println(secondType);
    }
}
