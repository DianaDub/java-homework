package examples.lessons.ls12.generics.ex4;

import examples.lessons.ls11.interfaces.ex2.CrossFitMan;
import examples.lessons.ls12.generics.ex3.GenericHumanInside;
import examples.lessons.ls12.generics.ex3.Hobbit;
import examples.lessons.ls12.generics.ex3.Horseman;
import examples.lessons.ls12.generics.ex3.Humanable;
import examples.lessons.ls9.bear.Bear;

public class GenericsMethodClass {
    public static void main(String[] args) {
        printWhatever(new Bear("Miki", true));
        printWhateverWithTwoArguments(new CrossFitMan("Sam"), new Hobbit("Frodo"));
        workWithHumanable(new Horseman("Dino")); //буде працювати лише з Humanable
        printHumanable(new GenericHumanInside<>(new Horseman("Frog")));
    }
    public static <T> void printWhatever(T value){
        System.out.println(value);
    }
    public static <T, T2> void printWhateverWithTwoArguments(T value, T2 value2){
        System.out.println(value);
        System.out.println(value2);
    }
    //тобто, буде працювати лише з Humanable
    public static <T1 extends Humanable> void workWithHumanable(T1 instance){
        System.out.println(instance);
    }
    //якщо не хочемо вказувати конкретний тип, потрібно використовувати ?, тобто, під час використання методу вкажемо тип
    public static void printHumanable(GenericHumanInside<?> humanInside) {
        System.out.println(humanInside.getHumanable());
    }
//    public static <T> int returnIntWhatever(T value){
//        return (int) value;
//    }
}
