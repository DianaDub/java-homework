package examples.lessons.ls12.generics.ex3;

public enum Type {
    HUMAN, HOBBIT, ELF, HORSEMAN
}
