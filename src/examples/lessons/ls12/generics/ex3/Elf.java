package examples.lessons.ls12.generics.ex3;

public class Elf extends Humanable{
    public Elf(String name) {
        super(name);
        this.type = Type.ELF;
    }

    @Override
    protected void sayWhoAmI() {
        System.out.println("My name is " + name + ", my type is " + type);
    }
}
