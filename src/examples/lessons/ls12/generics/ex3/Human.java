package examples.lessons.ls12.generics.ex3;

public class Human extends Humanable{
    public Human(String name) {
        super(name);
        this.type = Type.HUMAN;
    }

    @Override
    protected void sayWhoAmI() {
        System.out.println("My name is " + name + ", my type is " + type);
    }
}
