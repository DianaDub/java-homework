package examples.lessons.ls12.generics.ex3;

import examples.lessons.ls11.interfaces.ex2.Sportable;

import javax.swing.plaf.PanelUI;

public class GenericHumanInside <T extends Humanable>{
    private T humanable;

    public T getHumanable() {
        return humanable;
    }

    public GenericHumanInside(T humanable) {
        this.humanable = humanable;
    }
    //без спадкування <T extends Humanable> не можна було б викликати метод .sayWhoAmI()
    public void sayInfoAboutInstance() {
        humanable.sayWhoAmI();
    }
}
