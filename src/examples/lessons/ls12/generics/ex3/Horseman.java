package examples.lessons.ls12.generics.ex3;

public class Horseman extends Humanable{
    public Horseman(String name) {
        super(name);
        this.type = Type.HORSEMAN;
    }

    @Override
    protected void sayWhoAmI() {
        System.out.println("My name is " + name + ", my type is " + type);
    }
}
