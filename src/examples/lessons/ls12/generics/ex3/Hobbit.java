package examples.lessons.ls12.generics.ex3;

public class Hobbit extends Humanable{
    public Hobbit(String name) {
        super(name);
        this.type = Type.HOBBIT;
    }

    @Override
    protected void sayWhoAmI() {
        System.out.println("My name is " + name + ", my type is " + type);
    }
}
