package examples.lessons.ls12.exception.ex3;

public class MiaNameException extends Exception{
    @Override
    public String getMessage(){
        return "Don't insert name Mia";
    }
}
