package examples.lessons.ls12.exception.ex3;

import java.util.Scanner;

public class PersonException {
    public static void main(String[] args) {
        PersonException person = new PersonException();
        try {
            person.name = person.askName();
        } catch (MiaNameException mia) {
            System.out.println(mia.getMessage());
        }
        System.out.println("Check");
    }
    public String name;
    public String askName() throws MiaNameException {
        System.out.println("Inset name for person");
        String name = new Scanner(System.in).nextLine();
        if (name.equals("Mia")) {
            throw new MiaNameException();
        } else {
            return name;
        }
    }
}
