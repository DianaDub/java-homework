package examples.lessons.ls12.exception.ex5;

public class MultipleCatchMainExample {
    public static void main(String[] args) {
        try {
            System.out.println("This is a code");
        } catch (ArithmeticException | NullPointerException e) {
            System.out.println("Exception");
        }
    }
}
