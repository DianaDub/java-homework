package examples.lessons.ls12.exception.ex4;

import examples.lessons.ls10.static_key_word.task.C;

public class Example4Task {
    public class OddLengthException extends Exception{
        public OddLengthException(String message) {
            super(message);
        }
    }
    public class ContainsWordsException extends Exception{
        public ContainsWordsException(String message) {
            super(message);
        }
    }
    public class LessThanThreeWordsException extends Exception{
        public LessThanThreeWordsException(String message) {
            super(message);
        }
    }
    private void checkString1 (String string) throws OddLengthException {
        if (string.length()%2 != 0){
            throw new OddLengthException("The amount of words is odd");
        }
    }
    private void checkString2 (String string) throws ContainsWordsException {
        if (string.contains("exception")){
            throw new ContainsWordsException("Your string contains word 'exception'");
        }
    }
    private void checkString3 (String string) throws LessThanThreeWordsException {
        if (string.split(" ").length < 3){
            throw new LessThanThreeWordsException("Your string contains less than three words");
        }
    }
    public void checkString4 (String string) throws OddLengthException, ContainsWordsException, LessThanThreeWordsException {
        checkString1(string);
        checkString2(string);
        checkString3(string);
        //можна не закидати в сигнатуру, а прописати tr->catch, але тоді воно сприйматиме як Exception,
        //і ми не побачимо нормального повідомлення
        /*try {
            checkString1(string);
            checkString2(string);
            checkString3(string);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }*/
    }
}
/*Створити клас із чотирма методами checkString().
Метод 1 приймає в параметри рядок і викидає виключення, якщо кількість символів в
цієї строки непарне.
Метод 2 приймає в параметри рядок і викидає виключення, якщо в цьому рядку є
буквосполучення "exception".
Метод 3 приймає в параметри рядок і викидає виключення, якщо цей рядок складається з кількості слів менше 3.
Метод 4 всередині себе викликає попередні три методи.
Усі виключення створити окремими власними класами.*/