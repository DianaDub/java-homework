package examples.lessons.ls12.exception.ex4;

public class MainCheckExceptionClass {
    public static void main(String[] args) {
        Example4Task example = new Example4Task();
        //example.checkString4("Some text");
        //або ж try->catch, або ж в сигнатуру додати, але в сигнатурі вибиватиме просто помилку без обробки
        /*try {
            example.checkString4("Some text");
        } catch (Example4Task.ContainsWordsException e) {
            System.out.println(e.getMessage());
        } catch (Example4Task.OddLengthException e) {
            System.out.println(e.getMessage());
        } catch (Example4Task.LessThanThreeWordsException e) {
            System.out.println(e.getMessage());
        }*/
        //виконується залежно від того, в якому порядку прописані методи в методі 4, якщо перше попало, то інші не виконуються
        //скорочено те ж саме
        try {
            example.checkString4("Some text5 exception");
        } catch (Example4Task.ContainsWordsException | Example4Task.OddLengthException | Example4Task.LessThanThreeWordsException e) {
            System.out.println(e.getMessage());
        }
    }
}
