package examples.lessons.ls12.exception.ex2;

import java.util.Scanner;

public class ExceptionExampleClass2 {
    public static void main(String[] args) {
       /* System.out.println(1);
        //потребує try -> catch
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(2);*/
        int [] array = {2, 8, 0, 11, 99};
        //
        System.out.println("Insert index of array: ");
        try {
            System.out.println(15/array[new Scanner(System.in).nextInt()]);
        } catch (ArrayIndexOutOfBoundsException indexException) {
            System.out.println("Away from index");
        } catch (ArithmeticException zeroException) {
            System.out.println("Division by zero is impossible");
        } catch (Exception exception) {
            System.out.println("Exception");
        }
        System.out.println("Everything is still working");
    }
}
