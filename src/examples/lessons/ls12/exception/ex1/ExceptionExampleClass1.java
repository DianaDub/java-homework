package examples.lessons.ls12.exception.ex1;

import examples.lessons.ls11.abstract_class.ex1.Train;
import examples.lessons.ls11.interfaces.ex1.Fish;
import examples.lessons.ls12.generics.ex1.Container;
import examples.lessons.ls12.generics.ex3.Elf;

import java.util.Scanner;

public class ExceptionExampleClass1 {
    public static void main(String[] args) {
        /*//ArithmeticException
        System.out.println(13/0);*/

        /*//ArrayIndexOutOfBoundsException
        int[] array = new int[3];
        System.out.println(array[1000]);*/

        /*//ClassCastException
        Container container1 = new Container("Text");
        Container container2 = new Container(33);
        System.out.println(container1.getObject() + (String)container2.getObject());*/

        /*//NullPointerException
        Fish fish = null;
        fish.eat();*/

        try {
            int number = 15;
            Scanner scanner = new Scanner(System.in);
            int number2 = scanner.nextInt();
            System.out.println(number/number2);
        } catch (ArithmeticException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
