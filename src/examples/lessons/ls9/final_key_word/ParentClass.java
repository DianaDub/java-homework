package examples.lessons.ls9.final_key_word;

public class ParentClass {
    public String text = "text";
    public final String finalText = "finalText";
    public void justMethod() {
        System.out.println("I am a justMethod");
    }
    public final void justFinalMethod() {
        System.out.println("I am a justFinalMethod");
    }
}
