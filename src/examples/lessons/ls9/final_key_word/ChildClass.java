package examples.lessons.ls9.final_key_word;

public class ChildClass extends ParentClass {
    @Override
    public void justMethod() {
        System.out.println("I can override justMethod");
    }
    /* так не можна теж
    @Override
    public final void justFinalMethod() {
        System.out.println("I can override justFinalMethod");
    }*/
}