package examples.lessons.ls9.final_key_word;

public class FinalMainClass {
    public static void main(String[] args) {
        ParentClass parentClass = new ParentClass();
        parentClass.text = "newText";
        //так не можна, бо він файнал
        //parentClass.finalText = "newFinalText";
        final String textFinal2 = "textFinal2";   // це константа
        System.out.println(textFinal2);
    }
}
