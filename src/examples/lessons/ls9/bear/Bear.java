package examples.lessons.ls9.bear;

public class Bear {
    public static void main(String[] args) {
        Bear bear = new Bear("Sam", true);
        Bear bear1 = new Bear("Mia", false);
        Bear bear2 = new Bear("Avid", true);
        Bear child1 = bear.makeBabyBear(bear1, "Olis", false);
        Bear child2 = bear.makeBabyBear(bear2, "Akim", true);
        System.out.println(child1);
        System.out.println(child2);
    }
    private String name;

    public Bear(String name, boolean isMale) {
        this.name = name;
        this.isMale = isMale;
    }

    private boolean isMale;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private int age;
    public void voice () {
        System.out.println("RRRRRRRR!!!!");
    }

    @Override
    public String toString() {
        if (name == null) {
            return "I can't be born";
        }
        else {
            return "Name of Bear = " + name +
                    ", age of Bear = " + age +
                    ", sex of Bear = " + isMale;
        }
    }

    public Bear makeBabyBear (Bear partnerBear, String newString, boolean newBoolean) {
        if (this.isMale == partnerBear.isMale) {
            return new Bear(null, false);
        }
        else {
            return new Bear(newString, newBoolean);
        }
    }
}
/*Створити клас Ведмедик. В ньому поля ім'я, вік і стать. Поля приватні.
Створити конструктор для задання імені і поля. Для Віку задати
гетери і сетери.
Створити метод голосу, який виводить у консоль повідомлення "RRRRRRRR!!!!"

Створіть метод makeBabyBear, який у своїх параметрах вимагає об'єкта
класу Bear, рядок і boolean.
Якщо стать у ведмедя співпадає, то повертається новий ведмідь з порожнім іменем.
(поле стать можете залишити будь-який). Якщо стать різна, то повертається новий ведмідь
з іменем і статтю, які він отримує з аргументів, переданих у параметри методу.

Перевизначити метод toString(), який у випадку, якщо у ведмедя поле імені рівне
null, повертає рядок типу: "Я не можу бути народженим", у протилежному випадку він
повертає інформацію про ведмедя.*/