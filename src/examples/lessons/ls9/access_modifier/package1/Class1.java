package examples.lessons.ls9.access_modifier.package1;

public class Class1 {
    public static void main(String[] args) {
        Class1 class1 = new Class1();
        System.out.println(class1.publicString);
    }

    public String getPublicString() {
        return publicString;
    }

    public void setPublicString(String publicString) {
        this.publicString = publicString;
    }

    public String getPrivateString() {
        return privateString;
    }

    public void setPrivateString(String privateString) {
        this.privateString = privateString;
    }

    public String getProtectedString() {
        return protectedString;
    }

    public void setProtectedString(String protectedString) {
        this.protectedString = protectedString;
    }

    public String getDefaultString() {
        return defaultString;
    }

    public void setDefaultString(String defaultString) {
        this.defaultString = defaultString;
    }

    public String publicString = "publicString";
    private String privateString = "privateString";
    protected String protectedString = "protectedString";
    String defaultString = "defaultString";
    public void printFieldsFromClass1 () {
        Class1 class1 = new Class1();
        System.out.println(class1.publicString);
        System.out.println(class1.privateString);
        System.out.println(class1.protectedString);
        System.out.println(class1.defaultString);
    }
}
