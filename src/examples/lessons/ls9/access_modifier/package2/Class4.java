package examples.lessons.ls9.access_modifier.package2;

import examples.lessons.ls9.access_modifier.package1.Class1;

public class Class4 extends Class1 {
    public void printFieldsFromClass11() {
        System.out.println(protectedString);
        System.out.println(publicString);
        //вище спадкується від особистого поля класу, тому є протектід, нижче новостворений об'єкт, тому тільки паблік
        Class1 class1 = new Class1();
        System.out.println(class1.publicString);
        //після створення геттерів і сеттерів доступ є
        System.out.println(class1.getDefaultString());
        System.out.println(class1.getPrivateString());
        System.out.println(class1.getProtectedString());
    }
}
