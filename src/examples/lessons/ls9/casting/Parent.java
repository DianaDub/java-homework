package examples.lessons.ls9.casting;

public class Parent {
    public String name;
    public int age;
    public void doSomething() {
        System.out.println("I am a Parent. I do adult things");
    }
}
