package examples.lessons.ls9.casting;

public class CastingMain {
    public static void main(String[] args) {
        //це неявний кастинг
        Parent child1 = new Child();
        child1.doSomething();
        Parent child2 = new OtherChild();
        child2.doSomething();
        System.out.println();
        /* так не можна
        Child parent = (Child) new Parent();
        System.out.println(parent.toys);*/
        Child child3 = new Child();
        Parent parent = new Parent();
        Parent[] children = {child1, child2, child3, parent};
        for (Parent persons: children) {
            persons.doSomething();
        }
    }
}
