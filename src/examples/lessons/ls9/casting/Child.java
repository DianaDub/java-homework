package examples.lessons.ls9.casting;

public class Child extends Parent {
    public String[] toys;
    @Override
    public void doSomething() {
        System.out.println("I do child stuff");
    }
}
