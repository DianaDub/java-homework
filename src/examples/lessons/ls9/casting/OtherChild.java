package examples.lessons.ls9.casting;

public class OtherChild extends Parent {
    public String field;
    public String[] toys;
    @Override
    public void doSomething() {
        System.out.println("I do other child stuff");
    }
}
