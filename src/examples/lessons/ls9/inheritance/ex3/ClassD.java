package examples.lessons.ls9.inheritance.ex3;

public class ClassD extends ClassC {
    public ClassD () {
        System.out.println("I am a constructor from class D");
    }
}
