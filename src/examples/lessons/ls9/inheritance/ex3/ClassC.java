package examples.lessons.ls9.inheritance.ex3;

public class ClassC extends ClassB {
    public ClassC () {
        System.out.println("I am a constructor from class C");
    }
}
