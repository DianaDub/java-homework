package examples.lessons.ls9.inheritance.ex3;

public class ClassB extends ClassA {
    public ClassB () {
        System.out.println("I am a constructor from class B");
    }
}
