package examples.lessons.ls9.inheritance.ex2;

public class QA extends TeamMember {
    public QA (String name, int age) {
        super(name, age);
        this.profession = "Quality Assurance";
    }
    @Override
    public void doWork() {
        System.out.println("I am " + name + " and I am a good tester");
    }
}
