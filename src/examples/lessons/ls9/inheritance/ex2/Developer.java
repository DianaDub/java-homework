package examples.lessons.ls9.inheritance.ex2;

public class Developer extends TeamMember {
    public Developer () {
        super("Ann", 19);
        this.profession = "Developer";
    }
    @Override
    public void doWork() {
        System.out.println("I am " + name + " and I am a good creator");
    }
}
