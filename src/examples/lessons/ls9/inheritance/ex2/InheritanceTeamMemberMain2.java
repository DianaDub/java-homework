package examples.lessons.ls9.inheritance.ex2;

public class InheritanceTeamMemberMain2 {
    public static void main(String[] args) {
        QA qa = new QA("Sam", 21);
        System.out.println(qa);
        Developer developer = new Developer();
        System.out.println(developer);
        TeamMember teamMember = new TeamMember("Jenny", 17);
        System.out.println(teamMember);
        teamMember.doWork();
        developer.doWork();
        qa.doWork();
    }
}
