package examples.lessons.ls6;

import java.util.Scanner;

public class sixth_figures {
    /*Користувач вводить з клавіатури число, а на консолі виводиться квадрат із
    зірочок зі стороною рівною цьому числу
Приклад: Користувач ввів 5, вивід у консоль:
* * * * *
* * * * *
* * * * *
* * * * *
* * * * *
*/
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert some number: ");
        int number = scanner.nextInt();
        char symbol = '*';
        for (int i = 0; i < number; i++) {
            for (int j = 0; j < number; j++) {
                System.out.print(symbol + " ");
            }
            System.out.println();
        }
    }
    /*

     *
     * *
     * * *
     * * * *
     * * * * *

     * * * * *
     * * * *
     * * *
     * *
     *

     *
     * *
     * * *
     * * * *
     * * * * *

     */
}
