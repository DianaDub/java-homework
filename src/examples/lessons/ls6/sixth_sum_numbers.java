package examples.lessons.ls6;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class sixth_sum_numbers {
    public static void main(String[] args) {
        int [] numbers = new int[10];
        Random random = new Random();
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            numbers [i] = random.nextInt(-50, 50);
            sum = sum + numbers[i];
        }
        System.out.println(Arrays.toString(numbers));
        System.out.println(sum);
            /*1) Заповнити масив на 10 елементів випадковими числами від -50 до +50.
            Вивести в консоль сам масив. Вивести в консоль суму всіх його елементів.
         */
    }
}

