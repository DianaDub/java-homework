package examples.lessons.ls6.lesson_methods;

import java.util.Arrays;
import java.util.Random;

public class Lesson5Main {
    public static void main(String[] args) {
        Random random = new Random();
        int[] arrayRandom = new int[10];
        for(int i = 0; i < arrayRandom.length; i++){
            arrayRandom[i] = random.nextInt(-30, 30);
        }
        System.out.println(Arrays.toString(arrayRandom));
        System.out.println("Minimum from array " + Methods.returnMinimumFromArray(arrayRandom));
        Methods.arraySort(arrayRandom);
        System.out.println(Arrays.toString(arrayRandom));
    }
}
