package examples.lessons.ls6.lesson_methods;

import java.util.Arrays;

public class Methods {
    public static void arraySort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min;
            for (int a = i + 1; a < array.length; a++) {
                if (array[i] > array[a]) {
                    min = array[a];
                    array[a] = array[i];
                    array[i] = min;
                }
            }
        }
    }
    public static int[] reverseSort (int [] array) {
        int[] aReverse = new int[array.length];
        int counter = 0;
        for (int i = array.length - 1; i > -1; i--) {
            aReverse[counter] = array[i];
            counter++;
        }
        return aReverse;
    }
    public static int returnMinimumFromArray (int [] array) {
        int minimum = array[0];
        for(int i = 0; i < array.length; i++){
            if(minimum > array[i]){
                minimum = array[i];
            }
        }
        return minimum;
    }
    public static String [] addElementToArray (String[] array, String element) {
        String [] words4 = Arrays.copyOf(array, array.length + 1);
        words4[words4.length-1] = element;
        return words4;
    }
}
