package examples.lessons.ls6.lesson_methods;

import java.lang.reflect.AnnotatedArrayType;
import java.util.Arrays;
import java.util.SplittableRandom;

public class Arrays_Methods {
    public static void main(String[] args) {
        //sort of array
        int[] a = {34, 567, -12, 0, 564, 64, 766, -99, 99, 89, 11, 21};
        Arrays.sort(a);
        System.out.println(Arrays.toString(a));
        //sort reverse
        /*int[] aReverse = new int[a.length];
        int counter = 0;
        for (int i = a.length - 1; i > -1; i--) {
            aReverse[counter] = a[i];
            counter++;
        }
        System.out.println(Arrays.toString(aReverse));*/
        a = Methods.reverseSort(a);
        System.out.println(Arrays.toString(a));
        //Methods.arraySort(a);
        //System.out.println(Arrays.toString(a));
        String[] words = {"one", "two", "three", "four", "five"};
        Arrays.sort(words);
        System.out.println(Arrays.toString(words));

        //fill
        String [] strings = new String[10];
        Arrays.fill(strings, "a");
        System.out.println(Arrays.toString(strings));

        //equals
        int [] array1 = {3, 5, 10, 20, 0};
        int [] array2 = {3, 5, 10, 20, 0};
        System.out.println(array1.equals(array2));   //false
        System.out.println(Arrays.equals(array1,array2));   //true

        //deepToString - вивід у консоль, deepEquals - порівняння
        int [][] numbers1 = {{1, 2}, {3, 4}, {5, 6}};
        System.out.println(Arrays.deepToString(numbers1));
        int [][] numbers2 = {{1, 2}, {3, 4}, {5, 6}};
        System.out.println(Arrays.deepEquals(numbers1, numbers2));

        //copyOf = копіювання масиву
        String [] words2 = {"one", "two", "three", "four", "five"};
        String [] words3 = Arrays.copyOf(words2, words2.length);
        System.out.println(Arrays.toString(words3));
        //add new index
        String [] words4 = Arrays.copyOf(words2, words2.length + 1);
        words4[words4.length-1] = "six";
        System.out.println(Arrays.toString(words4));
        words2 = words4;
        System.out.println(Arrays.toString(words2));

        words4 = Methods.addElementToArray(words4, "seven");
        System.out.println(Arrays.toString(words4));

        //System.arraycopy() - спеціальний метод для копіювання масивів
        //перший параметр - це масив з якого потрібно копіювати дані,
        //другий параметр - індекс елемента масиву, з якого ми починаємо копіювати
        //третій параметр - масив, в якому ми будемо залишати елементи
        //четвертий параметр -індекс елемента масиву, з якого ми починаємо залишати скопійовані дані
        //п'ятий параметр - кількість скопійованих елементів
        int [] arrayParent = {1, 5, 34, 67, 99};
        int [] arrayChild = new int[arrayParent.length];
        System.arraycopy(arrayParent, 2, arrayChild, 1, 3);
        System.out.println(Arrays.toString(arrayChild));

        //compare
        int [] compare1 = {1, 5, 34, 67, 99};
        int [] compare2 = {1, 5, 34, 67, 99};
        int [] compare3 = {1, 5, 34, 99};
        System.out.println(Arrays.compare(compare1, compare2));
        System.out.println(Arrays.compare(compare1, compare3));
    }
}
