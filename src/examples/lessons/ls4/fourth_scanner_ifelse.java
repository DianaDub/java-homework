package examples.lessons.ls4;
import java.util.Scanner;

public class fourth_scanner_ifelse {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Що вас цікавить");
        String fromLine = scan.nextLine();
        if (fromLine.equals("Profession")) {
            System.out.println("AQA");
        } else if (fromLine.equals("Language")) {
            System.out.println("Java");
        } else if (fromLine.equals("Group")) {
            System.out.println("QA-8");
        } else {
            System.out.println("Information not found...Sorry.");
        }
        scan.close();

    }


}
