package examples.lessons.ls10.task_for_lesson10;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class WeekPlanner {
 public String[][] getSchedule(){
  String[][] schedule = new String[7][2];
  schedule[0][0] = "Sunday";
  schedule[1][0] = "Monday";
  schedule[2][0] = "Tuesday";
  schedule[3][0] = "Wednesday";
  schedule[4][0] = "Thursday";
  schedule[5][0] = "Friday";
  schedule[6][0] = "Saturday";
  return schedule;
 }

 public static void main(String[] args) {
  WeekPlanner planner = new WeekPlanner();
  String[][] schedule = planner.getSchedule();
  planner.setGoals(schedule);
  System.out.println(Arrays.deepToString(schedule));
  planner.workWithSchedule(schedule);
 }
 public void  workWithSchedule(String [][] schedule){
  Scanner scanner = new Scanner(System.in);
  System.out.println("Insert day of a week:");
  String dayOfWeek = scanner.nextLine();
  while (!dayOfWeek.equalsIgnoreCase("exit")){
   if (dayOfWeek.contains("change") || dayOfWeek.contains("reschedule")) {
    String day = dayOfWeek.split(" ")[1];
    for (int i = 0; i < schedule.length; i++) {
     if (schedule[i][0].equalsIgnoreCase(day)) {
      System.out.println("Please, enter task for " + schedule[i][0]);
      schedule[i][1] = scanner.nextLine();
      break;
     }
    }
    dayOfWeek = day;
   }
   switch (dayOfWeek.toLowerCase()) {
    case "sunday" -> System.out.println(schedule[0][1]);
    case "monday" -> System.out.println(schedule[1][1]);
    case "tuesday" -> System.out.println(schedule[2][1]);
    case "wednesday" -> System.out.println(schedule[3][1]);
    case "thursday" -> System.out.println(schedule[4][1]);
    case "friday" -> System.out.println(schedule[5][1]);
    case "saturday" -> System.out.println(schedule[6][1]);
    default -> System.out.println("Sorry, I don't understand you, please try again.");
   }
   System.out.println("Insert day of a week:");
   dayOfWeek = scanner.nextLine();
  }
 }
 public void setGoals(String[][] schedule) {
  Scanner scanner = new Scanner(System.in);
  for (int i = 0; i < schedule.length; i++) {
   System.out.println("Insert task for " + schedule[i][0]);
   schedule[i][1] = scanner.nextLine();
  }
 }
}
/*Написати клас WeekPlanner
    У цьому класі написати метод getSchedule(), що повертає двомірний масив розміром 7х2
    У цьому масиві ініціалізуються елементи, які будуть відповідати за дні неділі.
    schedule[0][0] = "Sunday";
    schedule[1][0] = "Monday";
    schedule[2][0] = "Tuesday";
    schedule[3][0] = "Wednesday";
    schedule[4][0] = "Thursday";
    schedule[5][0] = "Friday";
    schedule[6][0] = "Saturday";
    Останні елементи масиву будуть відповідати за завдання в планувальнику на ці дні.
    Написати метод setGoals(), який приймає в параметрах двомірний масив розміром 7х2,
    і запитує за допомогою клавіатури задати все для кожного дня неділі, щоб вийшло як у цьому прикладі:
    schedule[0][1] = "do home work";
    schedule[1][1] = "go to courses; watch a film";
    і т.д.
    Написати метод workWithSchedule(), який використовує цикл і оператор switch, запитує
    у день тижня користувача з консолі в залежності від введення буде відпрацьовано таким чином:

    програма: Please, input the day of the week:
    пользователь вводит правильный день недели (наприклад, Monday)
    програма виводить на екран:  Your tasks for Monday: go to courses;  watch a film.;
    програма ідет на наступну ітерацію;
    програма: Please, input the day of the week:
    користувач вводить некоректний день неділі (наприклад, %$=+!11=4)
    програма виводить на екран Sorry, I don't understand you, please try again.;
    програма іде на наступну ітерацію до успішного введення;
    програма: Please, input the day of the week:
    користувач виводить команду виходу exit
    програма виходить з циклу і правильно завершує роботу.
    Зверніть увагу: програма повинна приймати команди як у нижньому реєстрі (monday), так і у верхньому (MONDAY)
    и учтите, что пользователь мог случайно после дня недели ввести пробел*/

/*
       Додати в логіку методу, щоб при введенні користувача фрази change
       [day of the week]  або reschedule [day of the week],
       програма запропонувала ввести нові завдання для цього дня неділі
       і зберегла їх у відповідному місці масиву, удалив при цьому старі завдання.
       Після цієї програми слідує наступна ітерація до входу exit.

        програма: Please, input the day of the week:
        Користувач вводить: change Monday

        Програма пропонує ввести нові завдання на понеділок: Будь ласка, введіть нові завдання на понеділок.

        Користувач вводить нові завдання в консоль;
        програма зберігає нові завдання у відповідній ячейці;
        програма пішла на нову ітерацію і при введенні понеділка видає вже нові завдання.*/
