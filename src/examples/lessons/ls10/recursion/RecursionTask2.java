package examples.lessons.ls10.recursion;

import java.util.Scanner;

public class RecursionTask2 {
    public static void main(String[] args) {
        RecursionTask2 sumA = new RecursionTask2();
        sumA.takeCredit();
        //sumA.credit(10000, 1000, 1);
    }
    private void credit(int sum, int monthlyPay, int counter) {
        sum = sum - monthlyPay;
        System.out.println("Month number " + counter + ". You need to pay " + sum + " more");
        if (sum <= 0) {
            System.out.println("Credit already done in " + counter + " month");
        } else {
            counter++;
            credit(sum, monthlyPay, counter);
        }
    }
    public void takeCredit() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert some of money what you wanna take: ");
        int sumOfMoney = scanner.nextInt();
        System.out.println("Insert some of money what you wanna pay every month: ");
        int monthlyPay = scanner.nextInt();
        credit(sumOfMoney, monthlyPay, 1);
    }
}
//Користувач вводить з клавіатури суму, яку він хоче взяти в кредит, а також суму
//которую він буде виплачувати кожен місяць. В консоль виводиться порядковий номер місяця,
// і сума, яка залишається для виплати кредиту. Яяк тільки кредит виплачено, в консоль виводиться
//фраза: Кредит виплачено за {кіл-ть місяців}.