package examples.lessons.ls10.recursion;

public class RecursionTask1 {
    public static void main(String[] args) {
        RecursionTask1 numberR = new RecursionTask1();
        numberR.printAllNumbers(-0);
    }
    private void printAllNumbers(int number) {
        if (number == 0) {
            System.out.println(number);
        } else if (number < 0) {
            System.out.println("Your number is below 0");
        }
        else {
            System.out.println(number);
            number--;
            printAllNumbers(number);
        }
    }
}
//Користувач вводить позитивне число з клавіатури,
// система виводить в консоль всі числа від введеного,
//до 0. При рішенні даного завдання попробувати використовувати рекурсію