package examples.lessons.ls10.static_key_word.task;

public class A {
    public A () {
        System.out.println("Constructor from class A");
    }
    static {
        System.out.println("Static block class A");
    }
}
