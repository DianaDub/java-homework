package examples.lessons.ls10.static_key_word.task;

import examples.lessons.ls10.static_key_word.blocks.ClassWithStaticBlocks;

public class MainForTaskStatic {
    public static void main(String[] args) {
        D d = new D();
        System.out.println(d);
    }
}
