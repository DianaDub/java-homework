package examples.lessons.ls10.static_key_word.task;

public class D extends C {
    public D () {
        System.out.println("Constructor from class D");
    }
    static {
        System.out.println("Static block class D");
    }
}
