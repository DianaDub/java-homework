package examples.lessons.ls10.static_key_word.task;

public class C extends B {
    public C () {
        System.out.println("Constructor from class C");
    }
    static {
        System.out.println("Static block class C");
    }
}
