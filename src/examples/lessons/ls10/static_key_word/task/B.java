package examples.lessons.ls10.static_key_word.task;

public class B extends A{
    public B () {
        System.out.println("Constructor from class B");
    }
    static {
        System.out.println("Static block class B");
    }
}
