package examples.lessons.ls10.static_key_word.fields_methods;

public class MainForExampleStatic {
    public static void main(String[] args) {
        ExampleStatic exampleStatic = new ExampleStatic();
        System.out.println(exampleStatic.nonStaticString);
        System.out.println(ExampleStatic.staticString);
        ExampleStatic.staticMethod();
        exampleStatic.nonStaticMethod();
        //так працюватиме, але це не зовсім правильно
        System.out.println(exampleStatic.staticString);
        exampleStatic.staticMethod();
        //так не працюватиме
        //System.out.println(ExampleStatic.nonStaticString);
        //ExampleStatic.nonStaticMethod();
        System.out.println();
        System.out.println("Non static method can use non static and static: ");
        exampleStatic.printInfoWithHelpOfNoneStaticMethod();
        System.out.println();
        ExampleStatic.printInfoWithHelpOfStaticMethod();
        System.out.println();
        //можна, але не правильно
        exampleStatic.printInfoWithHelpOfStaticMethod();
        //так не можна
        //ExampleStatic.printInfoWithHelpOfNoneStaticMethod();
    }
}
