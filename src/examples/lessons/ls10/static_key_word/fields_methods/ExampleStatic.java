package examples.lessons.ls10.static_key_word.fields_methods;

public class ExampleStatic {
    public static String staticString = "staticString";
    public String nonStaticString = "nonStaticString";
    public void nonStaticMethod () {
        System.out.println("nonStaticMethod");
    }
    public static void staticMethod() {
        System.out.println("staticMethod");
    }
    public void printInfoWithHelpOfNoneStaticMethod() {
        //у нестатичних методах можна використовувати статичні і нестатичні методи,
        // змінні, тощо
        staticMethod();
        nonStaticMethod();
        System.out.println(staticString);
        System.out.println(nonStaticString);
    }
    public static void printInfoWithHelpOfStaticMethod() {
        staticMethod();
        System.out.println(staticString);
        //так не можна
        //nonStaticMethod();
        //System.out.println(nonStaticString);
    }
}
