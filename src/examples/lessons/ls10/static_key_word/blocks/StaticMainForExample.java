package examples.lessons.ls10.static_key_word.blocks;

public class StaticMainForExample {
    public static void main(String[] args) {
        /*ClassWithStaticBlocks classWithStaticBlocks = new ClassWithStaticBlocks();
        ClassWithStaticBlocks classWithStaticBlocks1 = new ClassWithStaticBlocks();*/
        //ClassWithStaticBlocks.justBe();
        //це вже не відпрацює
        //ClassWithStaticBlocks classWithStaticBlocks = new ClassWithStaticBlocks();
        /*System.out.println(ClassWithStaticBlocks.EURO);
        System.out.println(ClassWithStaticBlocks.USD);*/
        ClassWithStaticBlocks classWithStaticBlocks = new ClassWithStaticBlocks();
        classWithStaticBlocks.printCurrencies();
        classWithStaticBlocks.printADate();
    }
}
