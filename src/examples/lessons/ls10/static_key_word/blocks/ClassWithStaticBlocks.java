package examples.lessons.ls10.static_key_word.blocks;

import java.util.Date;

public class ClassWithStaticBlocks {
    static Date DATE;
    static double USD;
    static double EURO;
    public static double getUSDFromBank() {
        return 39.61;
    }
    public static double getEUROFromBank() {
        return 42.75;
    }
    static {
        System.out.println("I am a static block");
        USD = getUSDFromBank();
        EURO = getEUROFromBank();
        DATE = new Date();
    }
    public void printADate() {
        System.out.printf("%1$ta %1$td %1$tB", DATE);
    }
    public void printCurrencies() {
        System.out.println("EURO = " + EURO);
        System.out.println("USD = " + USD);
    }
    public static void justBe() {

    }
}
