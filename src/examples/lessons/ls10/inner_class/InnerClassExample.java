package examples.lessons.ls10.inner_class;

public class InnerClassExample {
    public static void main(String[] args) {
        Gym gym1 = new Gym("Sport");
        Gym gym2 = new Gym("Strong people");
        Gym.Equipment dumbbells = gym1.new Equipment("dumbbells", 3000);
        Gym.Equipment bar = gym1.new Equipment("bar", 200);
        Gym.Equipment ball = gym1.new Equipment("ball", 100);
        gym1.printEquipment();
        System.out.println();
        bar.sellToAnotherGym(gym2, 300);
        System.out.println("Gym 1");
        gym1.printEquipment();
        System.out.println("Gym 2");
        gym2.printEquipment();
        //Gym.Equipment bar = new Gym("Scrum").new Equipment("bar", 200);
        //System.out.println(dumbbells);
        //System.out.println(bar);
        gym2.addEquipment("circle", 1000);
        gym2.printEquipment();
    }
}
