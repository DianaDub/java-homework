package examples.lessons.ls1;

public class first_concat {
    public static void main(String[] args) {
        String word1 = "I";
        String word2 = "like";
        String word3 = "watching";
        String word4 = "movies";
        String word5 = "and series";
        String result1 = word1.concat(" ").concat(word2).concat(" ").concat(word3)
                .concat(" ").concat(word4).concat(" ").concat(word5);
        String result2 = word1 + " " + word2 + " " + word3 + " " + word4 + " " + word5;
        System.out.println(result1);
        System.out.println(result2);
    }
}
