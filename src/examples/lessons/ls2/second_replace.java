package examples.lessons.ls2;

public class second_replace {
    public static void main(String[] args) {
        String resumeStringName = "My name is {NAME}";
        String resumeStringCity = "I am from {CITY}";
        String result1 = resumeStringName.replace("{NAME}", "Diana") +
                ". " + resumeStringCity.replace("{CITY}", "Ovruch") + ".";
        System.out.println(result1);
        String result2 = resumeStringName.substring(0, resumeStringName.length()-6).concat
                ("Diana. ").concat(resumeStringCity.substring(0, resumeStringCity.length()-6)).
                concat("Ovruch.");
        System.out.println(result2);
    }
}
