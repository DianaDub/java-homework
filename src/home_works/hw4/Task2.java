package home_works.hw4;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        String numberString = String.valueOf(number);
        StringBuilder numberStringBuilder = new StringBuilder(numberString);
        String numberStringReverse = String.valueOf(numberStringBuilder.reverse());
        if (numberString.equals(numberStringReverse)) {
            System.out.println("Number is palindrome");
        }
        else {
            System.out.println(number + " is not palindrome");
        }
        scanner.close();
    }
}
//Написати програму в якій користувач вводить з клавіатури число, а програма визначає,
// є поліндромом чи ні. І виводить цю інформацію на екран.

