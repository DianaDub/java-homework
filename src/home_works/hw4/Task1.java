package home_works.hw4;

import java.util.Objects;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word1 = "";
        String word = scanner.nextLine();
        String stop = "STOP";
        while (!Objects.equals(word, stop)) {
            word1 = word1 + word + " ";
            System.out.println(word1);
            word = scanner.nextLine();
        }
        scanner.close();
    }
}
//Написати програму, яка зчитуватиме введені користувачем слова з клавіатури, і
//склеюватиме їх в одну пропозицію доти, доки користувач не введе з клавітури слово STOP.
//Всі слова, введені до цього, повинні відобразитися в консолі одним реченням.