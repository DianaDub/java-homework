package home_works.hw4;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int startWeight = 0;
        while (startWeight <= 500) {
            System.out.println("Insert weight of metal: ");
            int weight = scanner.nextInt();
            if (weight < 5) {
                System.out.println("Weight is too small");
                startWeight = startWeight;
                continue;
            }
            if (startWeight + weight > 500) {
                System.out.println("Limit exceeded.");
                System.out.println("You can insert only " + (500 - startWeight) + " kg");
                System.out.println("Insert weight of metal: ");
                weight = scanner.nextInt();
            }
            startWeight = startWeight + weight;
            if (startWeight == 500) {
                break;
            }
            if (startWeight > 495) {
                System.out.println("Your inserted weight = " + startWeight + " kg. You can't insert < 5 kg.");
                break;
            }

            System.out.println("You can insert only " + (500-startWeight) + " kg");
        }
        System.out.println("You have no place");
        scanner.close();
    }
}
//Написати програму, умовно для складу прийому металу. Припустимо, що склад може
//зберігати певну вагу металу. Користувач вводить з клавіатури вагу, яка може зберігатися
//на складі. Далі користувач вводить з клавіатури вагу, яку умовно збирається здати на склад
//користувач. Програма повинна після кожної здачі металу показувати скільки ваги може
//прийняти склад. Якщо користувач хоче здати металу більше ніж залишилося місця на складі,
//то програма не дає йому це зробити і повідомляє користувача про неможливість цієї операції.
//Якщо користувач здає метал вагою менше 5, програма теж попереджає про неможливість
//приймання такої малої ваги. Програма завершується, коли місце на складі закінчилось.