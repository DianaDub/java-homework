package home_works.hw4;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Random randomInt = new Random();
        int[] arrayInt = new int[10];
        for (int i = 0; i < arrayInt.length; i++) {
            arrayInt[i] = randomInt.nextInt(-10, 11);
        }
        System.out.println(Arrays.toString(arrayInt));
        Scanner scanner = new Scanner(System.in);
        int numberOfCustomer = scanner.nextInt();
        for (int i = 0; i < arrayInt.length; i++) {
            if (arrayInt[i] == numberOfCustomer) {
                System.out.println("Your number is in array");
                break;
            }
            continue;
        }
        scanner.close();
    }
}




//Є одновимірний масив із 10 елементів, заповнений випадковими числами. Користувач
//вводить із клавіатури число. Програма показує чи є таке число у створеному раніше масиві.