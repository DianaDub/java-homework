package home_works.hw4;

import java.util.Arrays;
import java.util.Random;

public class Task6 {
    public static void main(String[] args) {
        Random random = new Random();
        int [] arrayInt = new int[random.nextInt(45, 46)];
        for (int i = 0; i < arrayInt.length; i++) {
            arrayInt [i] = random.nextInt(-50, 51);
        }
        System.out.println("Amount of elements in array is " + arrayInt.length);
        System.out.println(Arrays.toString(arrayInt));
        int minimum = arrayInt[0];
        for (int i = 0; i < arrayInt.length; i++) {
            if (minimum > arrayInt[i]) {
                minimum = arrayInt[i];
            }
        }
        System.out.println("Minimum in array is " + minimum);
        int maximum = arrayInt[0];
        for (int i = 0; i < arrayInt.length; i++) {
            if (maximum < arrayInt[i]) {
                maximum = arrayInt[i];
            }
        }
        System.out.println("Maximum in array is " + maximum);
    }
}
//Заповнити масив на 45 елементів довільними числами від -50 до +50. Знайти мінімальний
//елемент та вивести його на консоль. Знайти максимальний елемент та вивести його на
//консоль.