package home_works.hw4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        String [] names = {"Петро", "Марія", "Олена", "Федір", "Олександр", "Антон", "Гліб"};
        String [] places = {"школу", "магазин", "церкву", "тренажерний зал", "кіно", "поліклініку"};
        String [] time = {"10", "12", "14", "16", "18", "20"};
        String result = "{NAME} буде йти в {PLACE} О {TIME}:00";
        int indexOfName = new Scanner(System.in).nextInt();
        while (indexOfName < 0 || indexOfName > 6) {
            System.out.println("Insert indexes from 0 to 6: ");
            indexOfName = new Scanner(System.in).nextInt();
        }
        int indexOfPlace = new Scanner(System.in).nextInt();
        while (indexOfPlace < 0 || indexOfPlace > 5) {
            System.out.println("Insert indexes from 0 to 5: ");
            indexOfPlace = new Scanner(System.in).nextInt();
        }
        int indexOfTime = new Scanner(System.in).nextInt();
        while (indexOfTime < 0 || indexOfTime > 5) {
            System.out.println("Insert indexes from 0 to 5: ");
            indexOfTime = new Scanner(System.in).nextInt();
        }
        System.out.println(result.replace("{NAME}", names[indexOfName]).replace("{PLACE}", places[indexOfPlace]).
                replace("{TIME}", time[indexOfTime]));
    }
}
//+ result.replace("{TIME}", time[indexOfTime])
//Перший складатиметься з наступних імен: “Петя”, “Маша”, “Олена”, “Федя”, “Саша”, “Антон”,
//“Гліб”. Другий міститиме такі значення типу int: 10, 12, 14, 16, 18, 20. Третій
//міститиме такі значення: “школу”, “магазин”, “церква”, “тренажерний зал”, “кіно”,
//“поліклініку” . Користувач вводить три числа з клавіатури, які відповідатимуть індексам
//кожного з елементів масивів. Приклад1. після введення 3,2,1: На екрані має вивестися
//наступне повідомлення: “Федя буде йти до магазину о 14:00” Приклад2. після
//введення 1,2,3: На екрані має вивестися наступне повідомлення:
//"Маша йтиме в тренажерний зал о 14:00"

