package home_works.hw10.task1;

public class Seller {
    public Seller(String name, boolean isHonest) {
        this.name = name;
        this.isHonest = isHonest;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHonest() {
        return isHonest;
    }

    public void setHonest(boolean honest) {
        isHonest = honest;
    }


    private boolean isHonest;
}