package home_works.hw10.task1;

public class Shop {
    private String name;

    public Shop(String name) {
        this.name = name;
    }

    public static void returnCost() {
        MethodFunction methodFunction = ((buyer, seller, productName, productCost, shop) -> {
            double fullSum = productCost;
            if (buyer.isDiscount()) {
                fullSum *= 0.9;
                if (productName.toLowerCase().contains("alcohol")) {
                    if (seller.isHonest() && buyer.getAge() < 21) {
                        System.out.println("Sorry, the honest seller " + seller.getName() + " will not sell alcohol to " + buyer.getName()
                                + ", because your age is " + buyer.getAge() + ".");
                    }
                    if (!seller.isHonest() && buyer.getAge() < 21) {
                        System.out.println("Dear, " + buyer.getName() + ", seller " + seller.getName() + " of our shop '" +
                                "Everything for you', will sell you this " + productName + ", it contains alcohol and your age is "
                                + buyer.getAge() + ". Full sum of your products is " + fullSum + ". Here is your discount.");
                    }
                    if (buyer.getAge() >= 21) {
                        System.out.println("Dear, " + buyer.getName() + ", seller " + seller.getName() + " of our shop '" +
                                "Everything for you', will sell you this " + productName + ", it contains alcohol and your age is "
                                + buyer.getAge() + ". Full sum of your products is " + fullSum + ". Here is your discount.");
                    }
                } else {
                    System.out.println("Your products don't contain alcohol, so our seller " + seller.getName() +
                            " will sell " + buyer.getName() + " products. Full sum is " + fullSum + ". Here your discount.");
                }
            } else {
                if (productName.toLowerCase().contains("alcohol")) {
                    if (seller.isHonest() && buyer.getAge() < 21) {
                        System.out.println("Sorry, the honest seller " + seller.getName() + " will not sell alcohol to " + buyer.getName()
                                + ", because your age is " + buyer.getAge() + ".");
                    }
                    if (!seller.isHonest() && buyer.getAge() < 21) {
                        System.out.println("Dear, " + buyer.getName() + ", seller " + seller.getName() + " of our shop '" +
                                "Everything for you', will sell you this " + productName + ", it contains alcohol and your age is "
                                + buyer.getAge() + ". Full sum of your products is " + fullSum + ". You don't have a discount.");
                    }
                    if (buyer.getAge() >= 21) {
                        System.out.println("Dear, " + buyer.getName() + ", seller " + seller.getName() + " of our shop '" +
                                "Everything for you', will sell you this " + productName + ", it contains alcohol and your age is "
                                + buyer.getAge() + ". Full sum of your products is " + fullSum + ". You don't have a discount.");
                    }
                } else {
                    System.out.println("Your products don't contain alcohol, so our seller " + seller.getName() +
                            " will sell " + buyer.getName() + " products. Full sum is " + fullSum + ". You don't have a discount.");
                }
            }
        });
        Buyer buyer = new Buyer("Max", 21, false);
        Seller seller = new Seller("Rob", true);
        Shop shop = new Shop("Everything for you");
        methodFunction.makeMethod(buyer, seller, "Alcohol", 210, shop);
    }
}