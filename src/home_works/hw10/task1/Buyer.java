package home_works.hw10.task1;

public class Buyer {
    private String name;
    private int age;

    public Buyer(String name, int age, boolean isDiscount) {
        this.name = name;
        this.age = age;
        this.isDiscount = isDiscount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isDiscount() {
        return isDiscount;
    }

    public void setDiscount(boolean discount) {
        isDiscount = discount;
    }

    private boolean isDiscount;

}