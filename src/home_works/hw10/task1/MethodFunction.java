package home_works.hw10.task1;

public interface MethodFunction {
    public void makeMethod(Buyer buyer, Seller seller, String productName, double productCost, Shop shop);
}
