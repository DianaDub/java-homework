package home_works.hw10.task2;

public class DivisionByZero extends Exception {
    public DivisionByZero(String message) {
        super(message);
    }
}
