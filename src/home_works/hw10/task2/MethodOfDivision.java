package home_works.hw10.task2;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MethodOfDivision {
    public static double resultOfDivision() throws IndexOfBoundException, DivisionByZero {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int size = random.nextInt(31);
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(0, 30);
        }
        System.out.println(Arrays.toString(array));
        System.out.println("Insert index of array from 0 to 30: ");
        int index = scanner.nextInt();
        if (index < 0 || index > 30) {
            throw new IndexOfBoundException("You inserted wrong index.");
        }
        if (array[0] == 0) {
            throw new DivisionByZero("The first element of array is zero. Division by zero is impossible");
        }
        return (double) array[index] / array[0];
    }
}
/*
2) Створити метод, у якому створюється одновимірний масив типу int довільного розміру від 1 до 30, в якому кожному
елементу масиву надається довільне значення від 0 до 30. Даний метод запитує користувача ввести з клавіатури індекс
випадково згенерованого масиву. Метод повертає значення типу double, яке виходить при розподілі елемента із зазначеним
індексом користувача на перший елемент даного масиву.
Продумати всі можливі виняткові ситуації, які можуть виникнути у цьому методі. Для кожної виняткової ситуації
створити власний клас checked винятки. Прописати у методі умови генерації даних винятків, і навіть вказати в сигнатурі
даного методу. Обробку виключення здійснити в сторонньому класі Main в якому потрібно викликати даний метод.*/