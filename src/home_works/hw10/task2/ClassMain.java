package home_works.hw10.task2;

import static home_works.hw10.task2.MethodOfDivision.resultOfDivision;

public class ClassMain {
    public static void main(String[] args) throws IndexOfBoundException, DivisionByZero {
        try {
            double result = resultOfDivision();
            System.out.println("Result of division: " + result);
        } catch (IndexOfBoundException e) {
            System.out.println("Invalid index: " + e.getMessage());
        } catch (DivisionByZero e) {
            System.out.println("Division by zero error: " + e.getMessage());
        }
    }
}
