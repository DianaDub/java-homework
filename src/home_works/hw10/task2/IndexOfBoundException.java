package home_works.hw10.task2;

public class IndexOfBoundException extends Exception {
    public IndexOfBoundException(String message) {
        super(message);
    }

}
