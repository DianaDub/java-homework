package home_works.hw10.task3;

public class BuyingMain {
    public static void main(String[] args) throws Shop.AllExceptions {
        Shop shop = new Shop();
        Father father = new Father("Max", 1000);
        Products product1 = Products.MILK;
        int quantity1 = 9;
        Products product2 = Products.POTATO;
        int quantity2 = 12;
        father.goShopping(shop, product1, quantity1, product2, quantity2);
    }
}
