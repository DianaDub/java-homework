package home_works.hw10.task3;

public class Shop {
    private int[] prices;

    //APPLE, POTATO, MILK, BEER, TOBACCO
    public Shop() {
        prices = new int[]{21, 19, 33, 31, 91};
    }

    class AllExceptions extends Exception {
        public AllExceptions(String message) {
            super(message);
        }
    }

    private int calculateOfProducts(Products product1, int quantity1, Products product2, int quantity2) {
        return prices[product1.ordinal()] * quantity1 + prices[product2.ordinal()] * quantity2;
    }

    public void buying(Products product1, int quantity1, Products product2, int quantity2, int sumOfMoney) throws AllExceptions {
        int totalSum = calculateOfProducts(product1, quantity1, product2, quantity2);
        if (totalSum > sumOfMoney) {
            throw new AllExceptions("You have less money");
        }
        if (product1 == Products.BEER || product1 == Products.TOBACCO
                || product2 == Products.BEER || product2 == Products.TOBACCO) {
            throw new AllExceptions("You shouldn't buy beer or tobacco");
        }
        if (quantity1 % 3 != 0 || quantity2 % 3 != 0) {
            throw new AllExceptions("You couldn't division your products for 3 people");
        }
        System.out.println("Dear buyer, the total sum of your products is " + totalSum +
                ". You bought " + product1 + " in quantity " + quantity1 + " and " + product2 + " in quantity " + quantity2);
    }
}