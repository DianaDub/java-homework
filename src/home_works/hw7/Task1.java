package home_works.hw7;

public class Task1 {
    public static void returnNumbers (int[] intNumbers) {
        int a;
        int b;
        System.out.println("Odd: ");
        for (int i = 0; i < intNumbers.length; i++) {
            if (intNumbers[i] % 2 != 0) {
                a = intNumbers[i];
                System.out.print(a + " ");
            }
        }
        System.out.println();
        System.out.println("Even: ");
        for (int i = 0; i < intNumbers.length; i++) {
            if (intNumbers[i] % 2 == 0) {
                b = intNumbers[i];
                System.out.print(b + " ");
            }
        }
    }
    public static void main(String[] args) {
        int[] k = {3, 4, 6, 7, 2, 111, 43, 6656, 223};
        returnNumbers(k);
    }
}
// Написати метод, який приймає як параметр масив цілих чисел. І виводить на екран усі парні числа списком,
// а також непарні числа списком.