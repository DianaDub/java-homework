package home_works.hw7;

import java.util.Arrays;

public class Task2 {
    public static void overLoadMethod () {
        System.out.println("I am empty");
    }
    public static void overLoadMethod (String a) {
        System.out.println(a);
    }
    public static void overLoadMethod (String[] stringArray) {
        String sentence = "";
        for (int i = 0; i < stringArray.length; i++) {
            String string = stringArray[i];
            sentence = sentence + string + " ";
        }
        System.out.println(sentence);
    }
    public static void overLoadMethod (int [] intArray) {
        int a = 0;
        for (int i = 0; i < intArray.length; i++) {
            int a1 = intArray[i];
            a = a+a1;
        }
        System.out.println("Sum of numbers from array is " + a);
    }
    public static void overLoadMethod (int c, String myString) {
        System.out.println("Your message is \"" + myString + "\", your number is \"" + c + "\"");
    }
    public static void main(String[] args) {
        overLoadMethod();
        overLoadMethod("It is a string");
        overLoadMethod(new String[] {"I", "am", "an", "array", "of", "strings"});
        overLoadMethod(new int [] {34, 56, 12, 45, 98});
        overLoadMethod(111, "My string");
    }
    /*Написати перевантажений метод, який може:
• Не приймати жодних значень, тоді він виводитиме на консоль повідомлення типу: "Я порожній".
• Приймати як параметри String, тоді він виводитиме на консоль це повідомлення.
• Приймати як параметри масив рядків, тоді він виводитиме на консоль усі його значення через пробіл.
• Приймати як параметр масив чисел, тоді він виводитиме на консоль суму елементів масиву.
• Приймати як параметри число і рядок, тоді він буде виводити на консоль повідомлення типу:
"Ваше повідомлення - "%%%%%%%%", ваше число - $", де "%%%%%%%%%" і $ ваші введені рядок та число відповідно.*/
}
