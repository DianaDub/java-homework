package home_works.hw9;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        if (trickLevel >= 0 && trickLevel <= 100) {
            this.trickLevel = trickLevel;
        } else {
            throw new IllegalArgumentException("Trick level must be between 0 and 100.");
        }
        this.habits = habits;
    }


    public Pet () {

    }
    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    private String species;
    private String nickname;
    private int age;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && trickLevel == pet.trickLevel && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }

    private int trickLevel;

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 0 && trickLevel <= 100) {
            this.trickLevel = trickLevel;
        }else {
            throw new IllegalArgumentException("Trick level must be between 0 and 100.");
        }
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    private String[] habits;
    public String eat() {
        return "I am eating";
    }
    public String respond() {
        return "Hello, host. I am " + nickname + ". I missed you!";
    }
    public String foul () {
        return "I need to cover my tracks well...";
    }
    public String getCunningLevel() {
        if (trickLevel >= 0 && trickLevel <= 50) {
            return "Not cunning";
        } else if (trickLevel > 50 && trickLevel <= 100) {
            return "Very cunning";
        } else {
            throw new IllegalStateException("Trick level is invalid.");
        }
    }
    @Override
    public String toString() {
        return species + "{nickname = \'" + nickname + "\', age = " + age + ", trickLevel = " + trickLevel +
                ", habits = " + Arrays.toString(habits) + "}";
    }
    public String  greetPet () {
        return "Hello, " + getNickname();
    }

    public String describePet () {
        String cunningLevel = (getTrickLevel() > 50) ? "very cunning" : "almost not cunning";
        return "I have a " + getSpecies() + ", it is " + getAge() +
                " years old, it is " + cunningLevel + "." ;
    }
}
/*
Опишіть у класі Pet такі поля:
• вид тварини (species), рядок (собака, кіт тощо)
• кличка (nickname)
• вік (age)
• Рівень хитрості (trickLevel) (ціле число від 0 до 100)
• звички (habits) (масив рядків)

Опишіть у класі Pet та реалізуйте такі методи:
• поїсти (eat) (метод виводить на екран повідомлення Я ї'м!)
• відгукнутися (respond) (метод виводить на екран повідомлення Привіт, хазяїн. Я - [ім'я тварини]. Я скучив!)
• зробити домашню гидоту (foul) (метод виводить на екран повідомлення Потрібно добре замести сліди...)

Перевизначте метод toString() в обох класів;
• Клас Pet повинен виводити повідомлення наступного виду: dog{nickname='Rock', age=5, trickLevel=75,
habits=[eat, drink, sleep]}, де dog - вид тварини;
У класі Pet створіть конструктори:
• конструктор, що описує вид тварини та її прізвисько
• конструктор, що описує всі поля тварини
• Порожній конструктор

Оголосіть усі поля наявних класів як приватні. Додайте гетери та сетери; зробіть рефакторинг наявних методів з
урахуванням змін.
*/