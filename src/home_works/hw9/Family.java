package home_works.hw9;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
    }
    public void addChild(Human child) {
        Human[] newChildren = Arrays.copyOf(children, children.length + 1);
        newChildren[newChildren.length - 1] = child;
        children = newChildren;
        child.setFamily(this);
    }
    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.length - 1) {
            return false;
        }
        Human[] newChildren = new Human[children.length - 1];
        int newIndex = 0;
        for (int i = 0; i < children.length; i++) {
            if (i != index) {
                newChildren[newIndex] = children[i];
                newIndex++;
            }
        }
        return true;
    }
    public int countFamily() {
        return 2 + children.length;
    }
    private Pet pet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }


}
/*
Опишіть у класу Family наступні поля:
• мама mother (тип Human)
• тато father (тип Human)
• діти children (масив Human)
• домашній улюбленець pet (тип Pet)

Додайте гетери, сетери; перевизначте toString так щоб він показував всю інформацію про всіх членів сім’ї.
У класі Family створіть конструктор:\
• єдиною умовою створення нової сім'ї є наявність 2-х батьків, причому у батьків має встановлюватися
посилання на поточну нову сім'ю, а сім'я створюється з порожнім масивом дітей.

У Family опишіть методи:
• додати дитину addChild (приймає тип Human і додає її до масиву дітей; додає дитині посилання на поточну родину)
• видалити дитину deleteChild (приймає індекс масиву та видаляє даний елемент; повертає логічне значення -
чи був видалений елемент)
• отримати кількість осіб у сім'ї countFamily (батьки у сім'ї ніколи не змінюються; якщо відбувається зміна
батьківського складу – це вже інша родина)
• Перевизначте метод toString()

Зробіть рефакторинг класу Human з урахуванням структури Family:
• Видаліть з Human всю інформацію, що дублюється в класі Family (Human повинен зберігати тільки інформацію, що описує себе)
• додайте поле family (тип Family) (тепер воно зберігає всю інформацію про сімейні дані людини)
• додайте необхідні гетери, сетери, конструктори; видаліть неактуальні конструктори та методи; зробіть рефакторинг методів
привітати свого улюбленця, описати свого улюбленця, нагодувати
• Зробіть рефакторинг методу toString(). Він повинен виводити інформацію в наступному форматі: Human{name='Name',
surname='Surname', year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}.

 */