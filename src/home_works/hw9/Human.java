package home_works.hw9;

import java.util.Objects;

public class Human {
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }
    public Human () {

    }
    private String name;
    private String surname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        if (iq >=0 && iq <= 100){
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be from 0 to 100");
        }
    }

    public Pet getMyPet() {
        return myPet;
    }

    public void setMyPet(Pet myPet) {
        this.myPet = myPet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }
    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    private int year;
    private int iq;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(myPet, human.myPet) && Objects.equals(mother, human.mother) && Objects.equals(father, human.father) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, myPet, mother, father, family);
    }

    private Pet myPet;

    public Human(String name, String surname, int year, int iq, Pet myPet, Human mother, Human father, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        if (iq >=0 && iq <= 100){
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be from 0 to 100");
        }
        this.myPet = myPet;
        this.mother = mother;
        this.father = father;
        this.family = family;
    }

    private Human mother;
    private Human father;
    private Family family;

    @Override
    public String toString() {
        return "Human {name = \'" + name + "\', surname = \'" + surname + "\', year = " + year +
                ", iq = " + iq + ", mother = " + mother + ", father = " + father + ", pet = " + myPet + "}";
    }

}
/*
Опишіть у класу Human такі поля:
• Ім'я (name)
• Прізвище (surname)
• Рік народження (year), число
• Рівень IQ (iq) (ціле число від 0 до 100)
• Домашній улюбленець (pet) (об'єкт типу Pet)
• Мама (mother) (об'єкт типу Human)
• Тато (father) (об'єкт типу Human)

Опишіть у класу Human та реалізуйте такі методи:
• привітати свого улюбленця (greetPet)("Привіт, [ім'я тварини]")
• описати свого улюбленця (describePet): (вивести на екран всю інформацію про свого вихованця:
"У мене є [вид тварини], їй [вік тварини] років, він [дуже хитрий]/[майже не хитрий]". описи хитрості
повинна залежати від рівня хитрості вихованця, якщо понад 50 -[дуже хитрий], якщо менше або дорівнює 50 -
[майже не хитрий])

Перевизначте метод toString() в обох класів;
• Клас Human повинен виводити повідомлення наступного виду: Human{name='Michael', surname='Karleone', year=1977, iq=90, mother=Jane Karleone,
father=Vito Karleone, pet=dog{nickname=' Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}}

У класі Human створіть конструктори:
• конструктор, що описує ім'я, прізвище та рік народження
• конструктор, що описує ім'я, прізвище, рік народження, тата та маму
• конструктор, який описує всі поля
• Порожній конструктор
Оголосіть усі поля наявних класів як приватні. Додайте гетери та сетери; зробіть рефакторинг наявних методів з урахуванням змін.

Зробіть рефакторинг класу Human з урахуванням структури Family:
• Видаліть з Human всю інформацію, що дублюється в класі Family (Human повинен зберігати тільки інформацію, що описує себе)
• додайте поле family (тип Family) (тепер воно зберігає всю інформацію про сімейні дані людини)
• додайте необхідні гетери, сетери, конструктори; видаліть неактуальні конструктори та методи; зробіть рефакторинг методів
привітати свого улюбленця, описати свого улюбленця, нагодувати
• Зробіть рефакторинг методу toString(). Він повинен виводити інформацію в наступному форматі: Human{name='Name',
surname='Surname', year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}.

*/