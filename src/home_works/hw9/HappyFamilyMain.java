package home_works.hw9;

import java.util.Arrays;

public class HappyFamilyMain {
    public static void main(String[] args) {
        Pet pet1 = new Pet("dog", "Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human mother1 = new Human("Jane", "Karleone", 1956);
        Human father1 = new Human("Vito", "Karleone", 1955);
        Family family1 = new Family(mother1, father1);
        Human human1 = new Human("Michael", "Karleone", 1977, 90, pet1, mother1, father1, family1);
        System.out.println();
        Pet pet2 = new Pet("cat", "Mia", 1, 90, new String[]{"play", "eat", "drink", "sleep"});
        Human mother2 = new Human("Sara", "Mars", 1970);
        Human father2 = new Human("Dean", "Mars", 1973);
        Family family2 = new Family(mother2, father2);
        Human human2 = new Human("Marry", "Mars", 1992, 85, pet2, mother2, father2, family2);
        Human human7 = new Human("Stefan", "Mars", 1993, 95, pet2, mother2, father2, family2);
        family2.addChild(human2);
        family2.addChild(human7);
        Pet pet3 = new Pet();
        Pet pet4 = new Pet("mouse", "Sammy");
        Human mother3 = new Human("Lin", "Loud", 1988);
        Human father3 = new Human("Linkoln", "Loud", 1987);
        Human human3 = new Human("Lora", "Loud", 2002, mother3, father3);
        Family family3 = new Family(mother3, father3);
        Human child = new Human("Marta", "Mouth", 2007);
        family3.addChild(child);
        System.out.println("=====Add Marta to family3");
        System.out.println(family3);
        System.out.println("=====Family2");
        System.out.println(family2);
        System.out.println("=====Amount of members in family2");
        System.out.println(family2.countFamily());
        System.out.println("=====Information about Michael Karleone");
        System.out.println(human1.toString());
        System.out.println("=====Information about Marry Mars");
        System.out.println(human2.toString());
        System.out.println("=====Michael == Marry");
        System.out.println(human1.equals(human2));
        System.out.println("=====Family of Michael Karleone");
        System.out.println(human1.getFamily());
        System.out.println("=====Dog == Cat");
        System.out.println(pet1.equals(pet2));
        System.out.println("=====Cunning level of dog and other methods of pet");
        System.out.println(pet1.getCunningLevel());
        System.out.println(pet1.toString());
        System.out.println(pet1.respond());
        System.out.println(pet1.describePet());
        System.out.println(pet1.greetPet());
        System.out.println(pet1.eat());
        System.out.println(pet1.foul());
    }
}
/*
Міні проєкт “Щаслива родина”
Суть проєкту: опис структури сім’ї з урахуванням того, що кожна людина має кровних родичів, а склад сім’ї може з
часом змінюватися. До проходження теми “Колекції” всі домашні завдання повинні бути виконані з використанням
масивів (НЕ використовуйте інтерфейси List, Set, Map, поки про це не буде прямо сказано у завданні). Об’єкти
Завдання
• Створіть та опишіть класи: Домашній улюбленець (Pet), Людина (Human). Створіть конструктори для написаних класів.
 Створіть клас Main та у ньому створіть екземпляри описаних класів.
• Зробіть поля всіх класів приватними.
• Створіть та опишіть клас Сім'я (Family). Вважатимемо, що сім'я може створюватися двома людьми, які не розходяться
і можуть народжувати/усиновлювати своїх дітей. Діти можуть дорослішати та йти з сім'ї, створюючи свою власну.
• Перевизначте методи equals() та hashCode() у всіх класах проєкту.
• У класі Family додайте метод deleteChild(Human child) з урахуванням наявності даних методів.

Технічні вимоги:
Опишіть у класі Pet такі поля:
• вид тварини (species), рядок (собака, кіт тощо)
• кличка (nickname)
• вік (age)
• Рівень хитрості (trickLevel) (ціле число від 0 до 100)
• звички (habits) (масив рядків)

Опишіть у класі Pet та реалізуйте такі методи:
• поїсти (eat) (метод виводить на екран повідомлення Я ї'м!)
• відгукнутися (respond) (метод виводить на екран повідомлення Привіт, хазяїн. Я - [ім'я тварини]. Я скучив!)
• зробити домашню гидоту (foul) (метод виводить на екран повідомлення Потрібно добре замести сліди...)

Опишіть у класу Human такі поля:
• Ім'я (name)
• Прізвище (surname)
• Рік народження (year), число
• Рівень IQ (iq) (ціле число від 0 до 100)
• Домашній улюбленець (pet) (об'єкт типу Pet)
• Мама (mother) (об'єкт типу Human)
• Тато (father) (об'єкт типу Human)

Опишіть у класу Human та реалізуйте такі методи:
• привітати свого улюбленця (greetPet)("Привіт, [ім'я тварини]")
• описати свого улюбленця (describePet): (вивести на екран всю інформацію про свого вихованця: "У мене є [вид тварини], їй [вік тварини] років, він [дуже хитрий]/[майже не хитрий]". описи хитрості повинна залежати від рівня хитрості вихованця, якщо понад 50 -[дуже хитрий], якщо менше або дорівнює 50 -[майже не хитрий])

Перевизначте метод toString() в обох класів;
• Клас Pet повинен виводити повідомлення наступного виду: dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}, де dog - вид тварини;
• Клас Human повинен виводити повідомлення наступного виду: Human{name='Michael', surname='Karleone', year=1977, iq=90, mother=Jane Karleone, father=Vito Karleone, pet=dog{nickname=' Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}}

У класі Pet створіть конструктори:
• конструктор, що описує вид тварини та її прізвисько
• конструктор, що описує всі поля тварини
• Порожній конструктор
У класі Human створіть конструктори:
• конструктор, що описує ім'я, прізвище та рік народження
• конструктор, що описує ім'я, прізвище, рік народження, тата та маму
• конструктор, який описує всі поля
• Порожній конструктор
Оголосіть усі поля наявних класів як приватні. Додайте гетери та сетери; зробіть рефакторинг наявних методів з урахуванням змін.
Опишіть у класу Family наступні поля:
• мама mother (тип Human)
• тато father (тип Human)
• діти children (масив Human)
• домашній улюбленець pet (тип Pet)

Додайте гетери, сетери; перевизначте toString так щоб він показував всю інформацію про всіх членів сім’ї.
У класі Family створіть конструктор:\
• єдиною умовою створення нової сім'ї є наявність 2-х батьків, причому у батьків має встановлюватися посилання на поточну нову сім'ю, а сім'я створюється з порожнім масивом дітей.

Зробіть рефакторинг класу Human з урахуванням структури Family:
• Видаліть з Human всю інформацію, що дублюється в класі Family (Human повинен зберігати тільки інформацію, що описує себе)
• додайте поле family (тип Family) (тепер воно зберігає всю інформацію про сімейні дані людини)
• додайте необхідні гетери, сетери, конструктори; видаліть неактуальні конструктори та методи; зробіть рефакторинг методів
привітати свого улюбленця, описати свого улюбленця, нагодувати
• Зробіть рефакторинг методу toString(). Він повинен виводити інформацію в наступному форматі: Human{name='Name',
surname='Surname', year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}.

У Family опишіть методи:
• додати дитину addChild (приймає тип Human і додає її до масиву дітей; додає дитині посилання на поточну родину)
• видалити дитину deleteChild (приймає індекс масиву та видаляє даний елемент; повертає логічне значення -
чи був видалений елемент)
• отримати кількість осіб у сім'ї countFamily (батьки у сім'ї ніколи не змінюються; якщо відбувається зміна
батьківського складу – це вже інша родина)
• Перевизначте метод toString()

Вирішіть, які поля варто використовувати для порівняння в методі equals() (наприклад, звички тварини можуть
змінюватися).
У класі Main створіть кілька сімей, щоб у кожного класу були використані всі можливі конструктори.
Виведіть інформацію про кожну людину на екран.
У класі Main створіть маму, тата, дитину та її домашню тварину. Надайте всі необхідні посилання
(у дитини на батьків та вихованця), щоб вийшла повноцінна сім’я. Викличте всі доступні методи у дитини
(включаючи метод toString()) та у його вихованця.
 */