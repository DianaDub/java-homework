package home_works.hw3;
import java.util.Scanner;


public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert your numbers and symbol:");
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        String symbol = scanner.next();
        int result;
        result = symbol.equals("+") ? number1 + number2 : symbol.equals("%") ? number1 %
                number2 : symbol.equals("/") ? number1/number2: symbol.equals("*") ?
                number1*number2: 0;
        System.out.println(result);
        scanner.close();
    }
}
//Написати програму, використовуючи тернарний оператор, де користувач вводить з клавіатури
// два числа і символ – + або % або / або *. На екран виводиться результат дії над двома
// введеними числами. Якщо користувач ввів щось окрім даних символів, необхідно вивести 0.
