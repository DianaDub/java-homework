package home_works.hw3;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert your numbers:");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int result;
        result = a>b ? a-b : b-a;
        System.out.println(result);
        scanner.close();
    }
}
//За допомогою тернарного оператора необхідно отримати різницю двох чисел, введених з
// клавіатури, і завжди віднімати від більшого менше. Вивести цю різницю в консоль.