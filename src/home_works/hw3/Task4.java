package home_works.hw3;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert number from 1 till 7:");
        int number = scanner.nextInt();
        if (number == 1) {
            System.out.println("Monday");
        } else if (number == 2) {
            System.out.println("Tuesday");
        } else if (number == 3) {
            System.out.println("Wednesday");
        } else if (number == 4) {
            System.out.println("Thursday");
        } else if (number == 5) {
            System.out.println("Friday");
        } else if (number == 6) {
            System.out.println("Saturday");
        } else if (number == 7) {
            System.out.println("Sunday");
        } else {
            System.out.println("It would be better if today was friday");
        }
        scanner.close();
    }
}
//(Використовувати оператори if-else-if) Користувач вводить з клавіатури
// числа: Якщо число дорівнює 1, виведення на консоль “Понеділок”; Якщо число дорівнює 2,
// виведення на консоль “Вівторок”; Якщо число дорівнює 3, виведення на
// консоль “Середовище”; Якщо число дорівнює 4, виведення на консоль “Четвер”; Якщо число
// дорівнює 5, виведення на консоль “П'ятниця”; Якщо число дорівнює 6, виведення на
// консоль “Субота”; Якщо число дорівнює 7, то виведення на консоль "Неділя"; В іншому
// випадку виводимо текст: "Краще б сьогодні була п'ятниця".