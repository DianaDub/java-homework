package home_works.hw3;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert sentence:");
        String string1, string2, string3, string4;
        string1 = scanner.next();
        string2 = scanner.next();
        string3 = scanner.next();
        string4 = scanner.next() + " " + scanner.next() + " " + scanner.next();
        System.out.println("string1 = " + string1);
        System.out.println("string2 = " + string2);
        System.out.println("string3 = " + string3);
        System.out.println("string4 = " + string4);
        scanner.close();
    }
}
//Ви маєте рядок “Я тестую чудово. Що ще потрібно?”, яку потрібно ввести з клавіатури
//у консоль.
//Необхідно за допомогою тільки методу next() класу Scanner (не використовуємо метод
//nextLine()) присвоїти змінним типу String наступні значення: string1 = Я string2 = тестую
//string3 = чудово string4 = Що ще потрібно? Крім string1, string2, string3, string4
//нових змінних створювати не можна.