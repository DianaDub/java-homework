package home_works.hw3;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What program do you need?");
        String program = scanner.nextLine();
        switch (program) {
            case ("IntelliJ IDEA"): {
                Scanner scannerOS = new Scanner(System.in);
                System.out.println("What OS do you need?");
                String OS = scannerOS.nextLine();
                switch (OS) {
                    case ("Linux"):
                        System.out.println("Link of Intellij IDEA for Linux");
                        break;
                    case ("MacOS"):
                        System.out.println("Link of Intellij IDEA for MacOS");
                        break;
                    case ("Windows"):
                        System.out.println("Link of Intellij IDEA for Windows");
                        break;
                    default:
                        System.out.println("OS not found");
                        break;
                }
                break;
            }
            case ("Git"): {
                Scanner scannerOS = new Scanner(System.in);
                System.out.println("What OS do you need?");
                String OS = scannerOS.nextLine();
                switch (OS) {
                    case ("Linux"):
                        System.out.println("Link of Git for Linux");
                        break;
                    case ("MacOS"):
                        System.out.println("Link of Git for MacOS");
                        break;
                    case ("Windows"):
                        System.out.println("Link of Git for Windows");
                        break;
                    default:
                        System.out.println("OS not found");
                        break;
                }
                break;
            }
            case ("Java"): {
                Scanner scannerOS = new Scanner(System.in);
                System.out.println("What OS do you need?");
                String OS = scannerOS.nextLine();
                switch (OS) {
                    case ("Linux"):
                        System.out.println("Link of Java for Linux");
                        break;
                    case ("MacOS"):
                        System.out.println("Link of Java for MacOS");
                        break;
                    case ("Windows"):
                        System.out.println("Link of Java for Windows");
                        break;
                    default:
                        System.out.println("OS not found");
                        break;
                }
                break;
            }
            default:
                System.out.println("Program not found");
                break;
        }
    }
}
//Використовуючи оператор switch написати програму, яка виводить на консоль посилання
// для завантаження програми. З вибору програм взяти: IntelliJ IDEA, Git, Java. З вибору
// ОС взяти: Linux, MacOS, Windows. Програма повинна запитати користувача, яка програма
// йому цікава, також запитати яку ОС він використовує, а після вивести в консоль
// необхідне посилання. Якщо програма з такою назвою не виводить повідомлення в консоль,
// про те, що такої програми не існує. Якщо зазначеної користувачем ОС немає, виводиться
// повідомлення в консоль, що такої ОС немає.