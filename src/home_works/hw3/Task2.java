package home_works.hw3;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert your numbers:");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        if (a < b+c && b < a+c && c < a+b) {
            System.out.println("You can build a triangle");
        } else {
            System.out.println("You can't build a triangle");
        }
        scanner.close();
    }
}
//Кожна сторона трикутника менша за суму двох інших сторін.
//a<b+c      b<a+c     c<a+b

//Користувач вводить з клавіатури три цілі значення. На екран виводиться інформація,
// чи можна з цих сторін побудувати трикутник. (Необхідно згадати правило побудови
// трикутника з трьох сторін).