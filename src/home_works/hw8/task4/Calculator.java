package home_works.hw8.task4;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        start();
    }
    public static void start () {
        System.out.println("Calculator is opened. Please, enter your number a: ");
        Scanner scanner = new Scanner(System.in);
        Scanner scanner1 = new Scanner(System.in);
        double a = scanner.nextDouble();
        System.out.println("Insert symbol(+, -, *, /): ");
        String symbol = scanner1.nextLine();
        System.out.println("Please, enter your number b: ");
        double b = scanner.nextDouble();
        if (symbol.equals("+")) {
            sum(a, b);
            start();
        }
        else if (symbol.equals("Stop")) {
            System.out.println("Calculator is closed.");
        }
        else if (symbol.equals("*")) {
            multiply(a, b);
            start();
        }
        else if (symbol.equals("-")) {
            minus(a, b);
            start();
        }
        else if (symbol.equals("/")) {
            division(a, b);
            start();
        }
        else {
            System.out.println("You inserted wrong symbol. Try again.");
            start();
        }
    }
    public static void sum(double a, double b){
        double resultSum = a + b;
        System.out.println(a + " + " + b + " = " + resultSum);
    }
    public static void minus (double a, double b){
        double resultMinus = a - b;
        System.out.println(a + " - " + b + " = " + resultMinus);
    }
    public static void multiply(double a, double b){
        double resultMultiply = a * b;
        System.out.println(a + " * " + b + " = " + resultMultiply);
    }
    public static void division(double a, double b){
        double resultDivision = a / b;
        System.out.println(a + " / " + b + " = " + resultDivision);
    }
}
/*Створити клас калькулятор.
У ньому створити методи:
sum,
minus,
multiply,
division.
(Додавання, віднімання, множення та поділ відповідно.)
Кожен метод приймає як параметри два значення типу double. І виводить у консоль результат дії.
Також у класі є метод старт. Який виводить повідомлення в консоль, що калькулятор запущено.
І пропонує ввести дію у вашій консолі.
Калькулятор повинен приймати лише такі типи дій:
2+4; - приклад синтаксису додавання;
5-6; - приклад синтаксису віднімання;
25*3; - Приклад синтаксису множення;
34/3; - Приклад синтаксису поділу;
Після введення на консоль виводиться відповідь цієї дії. І потім знову виводиться повідомлення
про пропозицію ввести дію.
Програма закривається після введення Stop. Перед закриттям на консоль має виводитись повідомлення:
"Калькулятор закрито".*/