package home_works.hw8.task3;

public class Converter {
    public static void main(String[] args) {
        System.out.println(Converter.convertToString(12));
        System.out.println(Converter.convertToDouble(-12.324));
        System.out.println(Converter.convertToInt('a'));
        System.out.println(Converter.convertToInt("1234"));
        System.out.println(Converter.convertToString('+'));
    }
    public Converter (String name) {

    }
    public static int convertToInt (byte bytes) {
        return bytes;
    }
    public static int convertToInt (short shorts) {
        return shorts;
    }
    public static int convertToInt (int ints) {
        return ints;
    }
    public static int convertToInt (long longs) {
        return (int) longs;
    }
    public static int convertToInt (char chars) {
        return chars;
    }
    public static int convertToInt (float floats) {
        return (int) floats;
    }
    public static int convertToInt (double doubles) {
        return (int) doubles;
    }
    public static int convertToInt (String text) {
        return Integer.parseInt(text);
    }
    public static void convertToInt (boolean booleans) {
        System.out.println("Inserted type is boolean");
    }
    public static double convertToDouble (byte bytes) {
        return bytes;
    }
    public static double convertToDouble (short shorts) {
        return shorts;
    }
    public static double convertToDouble (int ints) {
        return ints;
    }
    public static double convertToDouble (long longs) {
        return longs;
    }
    public static double convertToDouble (char chars) {
        return chars;
    }
    public static double convertToDouble (float floats) {
        return floats;
    }
    public static double convertToDouble (double doubles) {
        return doubles;
    }
    public static double convertToDouble (String text) {
        return Double.parseDouble(text);
    }
    public static void convertToDouble (boolean booleans) {
        System.out.println("Inserted type is boolean");
    }
    public static String convertToString (byte bytes) {
        return String.valueOf(bytes);
    }
    public static String convertToString (short shorts) {
        return String.valueOf(shorts);
    }
    public static String convertToString (int ints) {
        return String.valueOf(ints);
    }
    public static String convertToString (long longs) {
        return String.valueOf(longs);
    }
    public static String convertToString (char chars) {
        return String.valueOf(chars);
    }
    public static String convertToString (boolean booleans) {
        return String.valueOf(booleans);
    }
    public static int convertToString (float floats) {
        return (int) floats;
    }
    public static int convertToString (double doubles) {
        return (int) doubles;
    }
    public static int convertToString (String text) {
        return Integer.parseInt(text);
    }
}
/*Необхідно створити клас конвертер, який матиме методи конвертації примітивних типів даних:
метод convertToInt; (Конвертує byte, short, int, long, char, float, double, String).
При введенні boolean виводить повідомлення, що введено тип boolean.
convertToDouble; (Конвертує byte, short, int, long, char, float, double, String).
При введенні boolean виводить повідомлення, що введено тип boolean.
convertToString; (Конвертує byte, short, int, long, char, boolean, float, double, String).
Цей клас повинен мати лише один конструктор, у параметрах якого можна вказати його ім'я.
А також лише один метод геттер для отримання інформації про назву конвертру.
*/