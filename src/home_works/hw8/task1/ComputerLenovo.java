package home_works.hw8.task1;

import home_works.hw8.task1.Computer;

public class ComputerLenovo {
    public static void main(String[] args) {
        Computer computer = new Computer("Lenovo", 20000, 100000, 200000);
        System.out.println(computer);
        Computer computer1 = new Computer("iPad", 15000, 200000, 100000);
        Computer computer2 = new Computer("Lenovo", 19000, 100000, 200000);
        System.out.println(computer.equals(computer1));
        System.out.println(computer.equals(computer2));
    }
}
    /*1) Створити клас Computer з конструктором, що приймає параметри Марка тип String, ціна тип int, оперативна пам'ять тип int, та відеокарта тип int.
Перевизначити метод toString для виведення об'єкта класу на слід. вигляді:
"Створено PC.
Ім'я = марка.
Ціна = ціна.
Відеокарта = обсяг відеокарти.
ОЗУ = Об'єм оперативної пам'яті."
Усі поля класу Computer мають бути приватними. Також створіть публічні методи для отримання інформації про поля класу Computer. А також методи зміни його полів.
Перевизначте метод equals та метод hashCode для вашого класу. Зробіть так, щоб комп'ютери вважалися однаковими у випадку, якщо у них:
рівні значення полів марка, оперативна пам'ять та відеокарта.
В окремому класі створіть об'єкт класу комп'ютер і виведіть у консоль інформацію про ваш об'єкт.*/