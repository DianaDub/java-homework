package home_works.hw8.task1;

import java.util.Objects;

public class Computer {
    public Computer(String mark, int price, int operationalMemory, int videoCard){
        this.mark = mark;
        this.price = price;
        this.operationalMemory = operationalMemory;
        this.videoCard = videoCard;
    }
    private String mark;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Computer computer = (Computer) o;
        return operationalMemory == computer.operationalMemory && videoCard == computer.videoCard && Objects.equals(mark, computer.mark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mark, operationalMemory, videoCard);
    }

    private int price;

    public int getPrice() {
        return price;
    }

    public int getVideoCard() {
        return videoCard;
    }

    public void setVideoCard(int videoCard) {
        this.videoCard = videoCard;
    }

    public int getOperationalMemory() {
        return operationalMemory;
    }

    public void setOperationalMemory(int operationalMemory) {
        this.operationalMemory = operationalMemory;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    private int operationalMemory;

//    @Override
//    public String toString() {
//        return "Computer{" +
//                "price=" + price +
//                '}';
//    }

    private int videoCard;
    @Override
    public String toString (){
        return "PC is created. \n" + "Name = " + mark + "\nPrice = " + price + "\nVideo Card = " + videoCard + "\nVolume operational memory = " + operationalMemory;
    }

    /*1) Створити клас Computer з конструктором, що приймає параметри Марка тип String, ціна тип int,
    оперативна пам'ять тип int, та відеокарта тип int.
Перевизначити метод toString для виведення об'єкта класу ось так:
"Створено PC.
Ім'я = марка.
Ціна = ціна.
Відеокарта = обсяг відеокарти.
ОЗУ = Об'єм оперативної пам'яті."
Усі поля класу Computer мають бути приватними.
Також створіть публічні методи для отримання інформації про поля класу Computer. А також методи зміни його полів.
Перевизначте метод equals та метод hashCode для вашого класу.
Зробіть так, щоб комп'ютери вважалися однаковими у випадку, якщо у них:
рівні значення полів марка, оперативна пам'ять та відеокарта.
В окремому класі створіть об'єкт класу комп'ютер і виведіть у консоль інформацію про ваш об'єкт.*/
}
