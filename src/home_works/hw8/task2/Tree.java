package home_works.hw8.task2;

public class Tree {
    public void getInfoAboutTree (){
        System.out.println(toString());
    }
    @Override
    public String toString() {
        return "Type of tree is " + type +
                ", height = " + height +
                ", countOfSticks = " + countOfSticks +
                ", colour is " + colour + ".";
    }
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getCountOfSticks() {
        return countOfSticks;
    }

    public void setCountOfSticks(int countOfSticks) {
        this.countOfSticks = countOfSticks;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    private int height;
    private int countOfSticks;
    private String colour;
    //tree2
    public Tree (String type, int height, int countOfSticks, String colour) {
        this.type = type;
        this.height = height;
        this.countOfSticks = 13;
        this.colour = "green";
    }
    //tree3
    public Tree (int height, int countOfSticks, String colour, String type) {
        this.height = height;
        this.countOfSticks = countOfSticks;
        this.colour = colour;
        this.setType("fir");
    }
    //tree
    public Tree () {
       this.height = 350;
       this.countOfSticks = 29;
       this.colour = "yellow";
    }
    //tree1
    public Tree (String type) {
        this();
        this.type = type;
    }
}
    /*Створити клас Tree із перевантаженими конструкторами.

У класі Tree є поля: String type; int height, int countOfSticks, String colour;
Конструктор1 приймає параметри String type; int height і присвоює значення полям
countOfSticks значення 13, colour "Зелений".
Конструктор2 приймає в параметри int height, int countOfSticks,
String colour і присвоює значення полю type "ялиця".
Констуктор за замовчуванням 3, який надає height = 350, countOfSticks = 29, сolour = "Жовтий".
Конструктор4 який приймає параметри String type, і викликає всередині себе конструктор 3.
Створити в окремому класі main 4 об'єкти класу Tree використовуючи 4 створені конструктори.
*/

