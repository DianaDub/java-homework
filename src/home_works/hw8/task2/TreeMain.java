package home_works.hw8.task2;

public class TreeMain {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.getInfoAboutTree();
        Tree tree1 = new Tree("oak");
        tree1.getInfoAboutTree();
        Tree tree2 = new Tree("birch", 120, tree.getCountOfSticks(), tree.getColour());
        tree2.getInfoAboutTree();
        Tree tree3 = new Tree(12, 110, "brown", tree.getType());
        tree3.getInfoAboutTree();
    }
}
