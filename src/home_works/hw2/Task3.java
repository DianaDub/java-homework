package home_works.hw2;

public class Task3 {
    public static void main(String[] args) {
        String text = "Completely random text in English. In it, we just need to determine how many times the character 'a' occurs there. And we can use the split method and the length method.";
        String [] amountOfParts = text.split("A");
        int amount = amountOfParts.length;
        int amountOfLettersUpperCase = amount - 1;
        String [] amountOfParts2 = text.split("a");
        int amount2 = amountOfParts2.length;
        int amountOfLettersLowerCase = amount2 - 1;
        int amountOfLettersA = amountOfLettersLowerCase + amountOfLettersUpperCase;
        System.out.println("Amount of letters \"A\" and \"a\" = " + amountOfLettersA);
    }
}
//При застосуванні до масиву рядків поля length можна дізнатися скільки
//елементів знаходиться у масиві.
//Тобто якщо у вас є масив рядків типу: String[] arrayOfString; то при виклику поля lenth:
//int size = arrayOfString.length;
//можна отримати скільки символів знаходиться в даному масиві.
//Спробувати, використовуючи метод split та інформацію з приводу поля length, вивести
// на екран яку кількість разів символ 'a' зустрічається у рядку:
//"Completely random text in English. In it, we just need to determine how many times
// the character 'a' occurs there. And we can use the split method and the length method."
//
//Розв'язати це завдання з обліків верхнього та нижнього регістру символу 'a'.