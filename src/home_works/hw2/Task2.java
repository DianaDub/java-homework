package home_works.hw2;

public class Task2 {
    public static void main(String[] args) {
        String string = "Testing, is my favourite job";
        String [] words = string.split(" ");
        String word1 = words[0].substring(0,words[0].length()-1);
        String word2 = words[1];
        String word3 = words[2];
        String word4 = words[3];
        String word5 = words[4];
        System.out.println("word1 = " + word1 + ", length of word1 = " + word1.length());
        System.out.println("word2 = " + word2 + ", length of word2 = " + word2.length());
        System.out.println("word3 = " + word3 + ", length of word3 = " + word3.length());
        System.out.println("word4 = " + word4 + ", length of word4 = " + word4.length());
        System.out.println("word5 = " + word5 + ", length of word5 = " + word5.length());
        System.out.println(word1.length()>word2.length()&&word1.length()>word3.length()&&
                word1.length()>word4.length()&&word1.length()>word5.length());
    }
}
//Створити рядок string = "Testing, is my favourite job".
//Вивести на екран окремо кожне слово та довжину цього слова у вигляді:
//Слово1 = (значення слова), Довжина цього слова = (значення довжини слів).
//Слово2 = (значення слова), Довжина цього слова = (значення довжини слів).
//і т.д.
//Вивести на консоль true, якщо перше слово довше інших, в іншому випадку вивести false.