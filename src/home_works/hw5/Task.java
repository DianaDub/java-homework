package home_works.hw5;

import java.util.Random;
import java.util.Scanner;

public class Task {
    public static void main(String[] args) {
        int number = new Random().nextInt(100);
        System.out.println(number);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter your name: ");
        Scanner scanner1 = new Scanner(System.in);
        String name = scanner.nextLine();
        System.out.println(name);
        System.out.println("Let the game begin!");
        String result = "Congratulations, {name}!";
        System.out.println("Please, enter number: ");
        Scanner scanner2 = new Scanner(System.in);
        int numberCustomer = scanner2.nextInt();
        while (true) {
            if (numberCustomer > number) {
                System.out.println("Your number is too big. Please, try again...");
                numberCustomer = scanner2.nextInt();
            }
            else if (numberCustomer < number || numberCustomer == 0) {
                System.out.println("Your number is too small. Please, try again...");
                numberCustomer = scanner2.nextInt();
            }
            else {
                System.out.println(result.replace("{name}", name));
                break;
            }
        }
    }
}
/*Написати програму “числа”, яка загадує випадкове число та пропонує гравцеві його вгадати.

Технічні вимоги:
• За допомогою java.util.Random програма загадує випадкове число в
діапазоні[0-100] та пропонує гравцеві через консоль ввести своє ім’я,
яке зберігається в змінній name. • Перед початком на екран виводиться
текст: Let the game begin! • Сам процес гри обробляється у нескінченному
циклі. • Гравцеві пропонується ввести число в консоль, після чого програма
порівнює загадану кількість з тим, що ввів користувач. • Якщо введене
число менше загаданого, програма виводить на екран текст: Your number
is too small. Please, try again.. Якщо введене число більше за загадане,
то програма виводить на екран текст: Your number is too big. Please, try
again.. • Якщо введене число відповідає загаданому, то програма виводить
текст: Congratulations, {name}!*/