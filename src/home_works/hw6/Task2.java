package home_works.hw6;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Task2 {
    public static void printAim(String [][] aim) {
        for (String[] line: aim) {
            System.out.println(Arrays.toString(line));
        }
    }
    public static void shootAim(String [][] aim, int number1, int number2) {
        aim[number1][number2] = "*";
    }
    public static void main(String[] args) {
        String[][] aim = {{"0","1","2","3","4","5"},
                {"1","-","-","-","-","-"},
                {"2","-","-","-","-","-"},
                {"3","-","-","-","-","-"},
                {"4","-","-","-","-","-"},
                {"5","-","-","-","-","-"}};
        printAim(aim);
        int[] target = new int[2];
        target[0] = new Random().nextInt(1,6);
        target[1] = new Random().nextInt(1,6);
        //System.out.println(Arrays.toString(target));
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert number1 from 1 to 5:");
        int number1 = scanner.nextInt();
        System.out.println("Insert number2 from 1 to 5:");
        int number2 = scanner.nextInt();
        while (true) {
            if (number1 > aim.length || number1 < 1 || number1==6) {
                System.out.println("Your number do not fit. Try again.");
                System.out.println("Insert number1 from 1 to 5:");
                number1 = scanner.nextInt();
                System.out.println("Insert number2 from 1 to 5:");
                number2 = scanner.nextInt();
                continue;
            }
            if (number2 > aim.length || number2 < 1 || number2==6) {
                System.out.println("Your number do not fit. Try again.");
                System.out.println("Insert number1 from 1 to 5:");
                number1 = scanner.nextInt();
                System.out.println("Insert number2 from 1 to 5:");
                number2 = scanner.nextInt();
                continue;
            }
            if (number1==target[0] && number2==target[1]) {
                System.out.println("You have won!");
                aim[number1][number2] = "X";
                printAim(aim);
                break;
            }
            shootAim(aim, number1, number2);
            printAim(aim);
            System.out.println("Insert number1 from 1 to 5:");
            number1 = scanner.nextInt();
            System.out.println("Insert number2 from 1 to 5:");
            number2 = scanner.nextInt();
        }
    }
}
/*2) Написати програму “стрільба по цілі”.
Технічні вимоги:
• Даний квадрат 5х5, де програма випадковим чином ставить ціль.
• Перед початком гри на екран виводиться текст: All Set. Get ready to rumble!.
• Сам процес гри обробляється у нескінченному циклі.
• Гравцеві пропонується ввести лінію для стрільби; програма перевіряє, щоб
було введено число, і введена лінія знаходиться в межах ігрового поля (1-5).
У випадку, якщо гравець помилився, пропонує ввести число ще раз.
• Гравцю пропонується ввести стовпчик для стрільби (має проходити аналогічну
перевірку).
• Після кожного пострілу необхідно показувати оновлене ігрове поле у консолі.
Клітинки, куди гравець вже стріляв, слід зазначити як *.
• Гра закінчується при попаданні в ціль. Наприкінці гри вивести в консоль
фразу You have won!, а також ігрове поле.
• Уражену ціль відзначити як x.
• Завдання повинно бути виконане за допомогою масивів (НЕ використовуйте
інтерфейси List, Set, Map).
*/