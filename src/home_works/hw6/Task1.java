package home_works.hw6;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Task1 {
    public static int maximumFromArray(int[] array){
        int maximum = array[0];
        for(int a = 0; a < array.length; a++){
            if(maximum < array[a]){
                maximum = array[a];
            }
        }
        return maximum;
    }
    public static void main(String[] args) {
        Random random = new Random();
        Scanner input = new Scanner(System.in);
        System.out.println("Enter number of size1:");
        int size1 = input.nextInt();
        System.out.println("Enter number of size2:");
        int size2 = input.nextInt();
        int [][] arrayNumbers = new int[size1][size2];
        for(int i = 0; i < arrayNumbers.length;i++){
            for (int j = 0; j < arrayNumbers[i].length;j++) {
                arrayNumbers[i][j]  = random.nextInt(1000);
                System.out.print(arrayNumbers[i][j] + " ");
            }
            System.out.println();
        }
        int [] number = new int[arrayNumbers.length];
        for (int i = 0; i < number.length; i++) {
            number[i] = maximumFromArray(arrayNumbers[i]);
        }
        System.out.println(Arrays.toString(number));
    }
}
/* 1) Користувач визначає розмірність двовимірного масиву з клавіатури.
Масив заповнюється випадковими числами від 0 до 1000.
Необхідно створити одновимірний масив, що складається з максимальних значень
кожного окремого масиву, що входить у двовимірний.
Новий отриманий масив вивести на екран.*/